package com.rezmike.mvpauth.utils;

public class AppConfig {

    //urls
    public static final String BASE_URL = "http://207.154.248.163:8080/api/v1/";
    public static final String VK_BASE_URL = "https://api.vk.com/method/";
    public static final String FACEBOOK_BASE_URL = "https://graph.facebook.com/v2.8/";

    //network
    public static final int RETRY_REQUEST_BASE_DELAY = 500;
    public static final int MAX_CONNECTION_TIMEOUT = 5000;
    public static final int MAX_READ_TIMEOUT = 5000;
    public static final int MAX_WRITE_TIMEOUT = 5000;
    public static final int RETRY_REQUEST_COUNT = 5;

    //jobManager
    public static final int JOB_MIN_CONSUMER_COUNT = 1;
    public static final int JOB_MAX_CONSUMER_COUNT = 3;
    public static final int JOB_KEEP_ALIVE = 120;
    public static final int JOB_UPDATE_DATA_INTERVAL = 30;
    public static final int JOB_LOAD_FACTOR = 3;
    public static final int JOB_INITIAL_BACK_OFF_IN_MS = 1000;
}
