package com.rezmike.mvpauth.utils;

import com.rezmike.mvpauth.BuildConfig;

public class ConstantManager {

    public static final int REQUEST_PERMISSION_CAMERA = 100;
    public static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 101;

    public static final int REQUEST_PROFILE_PHOTO_GALLERY = 200;
    public static final int REQUEST_PROFILE_PHOTO_CAMERA = 201;

    public static final String FILE_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".fileprovider";
    public static final String LAST_MODIFIED_HEADER = "Last-Modified";
    public static final String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";
    //public static final String BASE_URL = BuildConfig.HOST;

    public static final String REALM_USER = "mihail.reznichenko.98@mail.ru";
    public static final String REALM_PASSWORD = "ReznichenkoMikeRealm";
    public static final String REALM_AUTH_URL = "http://192.168.10.13:9080/auth";
    public static final String REALM_DB_URL = "realm://192.168.10.13:9080/~/default";
}
