package com.rezmike.mvpauth.mvp.views;

public interface IView {

    boolean viewOnBackPressed();
}
