package com.rezmike.mvpauth.mvp.views;

import android.support.annotation.Nullable;

import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;

public interface IRootView extends IView {

    void showMessage(String message);

    void showMessage(int stringResId);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    void initDrawer(ProfileInfoDto profileInfo);

    boolean isAllGranted(String[] permissions, boolean allGranted);

    void showBasketIndicator();

    void changeMenuItem(int position, MenuItemHolder newItemHolder);

    @Nullable
    IView getCurrentScreen();
}
