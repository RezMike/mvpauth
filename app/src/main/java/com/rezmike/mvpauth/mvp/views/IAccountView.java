package com.rezmike.mvpauth.mvp.views;

import android.net.Uri;

import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.data.storage.dto.UserSettingsDto;

public interface IAccountView extends IView {

    void changeState();
    void showEditState();
    void showPreviewState();

    void showPhotoSourceDialog();

    ProfileInfoDto getProfileInfo();
    void updateProfileInfo(ProfileInfoDto profileInfo);

    UserSettingsDto getSettings();
    void initSettings(UserSettingsDto settings);

    void updateAvatarPhoto(Uri uri);
}
