package com.rezmike.mvpauth.mvp.views;

import android.view.View;

public interface IFabBuilder {

    IFabBuilder setVisable(boolean visable);

    IFabBuilder setImageResId(int imageResId);

    IFabBuilder setColorResId(int colorResId);

    IFabBuilder setListener(View.OnClickListener listener);

    void build();
}
