package com.rezmike.mvpauth.mvp.models;

import com.birbit.android.jobqueue.JobManager;
import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.components.DaggerModelComponent;
import com.rezmike.mvpauth.di.components.ModelComponent;
import com.rezmike.mvpauth.di.modules.ModelModule;

import javax.inject.Inject;

public abstract class AbstractModel {

    @Inject
    DataManager mDataManager;
    @Inject
    JobManager mJobManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = DaggerModelComponent.builder()
                    .modelModule(new ModelModule())
                    .build();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    //for tests
    public AbstractModel(DataManager dataManager, JobManager jobManager) {
        mDataManager = dataManager;
        mJobManager = jobManager;
    }
}
