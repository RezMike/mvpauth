package com.rezmike.mvpauth.mvp.presenters;

public interface IAuthPresenter {

    void clickOnLogin();
    void clickOnVk();
    void clickOnFacebook();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();

    void checkEmailValid(String email);
    void checkPasswordValid(String password);

    //social facade

    void initSocialSdk();
    void onSocialResult(Object res, SocialSdkType type);
    void onSocialError(Object res, SocialSdkType type);
    void onSocialCancel();

    enum SocialSdkType {
        VK,
        FACEBOOK,
        TWITTER
    }
}
