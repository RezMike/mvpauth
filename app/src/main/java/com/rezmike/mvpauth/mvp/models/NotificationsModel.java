package com.rezmike.mvpauth.mvp.models;

import com.rezmike.mvpauth.data.storage.realm.NotificationRealm;

import rx.Observable;

public class NotificationsModel extends AbstractModel {

    public Observable<NotificationRealm> getNotificationObs() {
        return mDataManager.getNotificationObs();
    }
}
