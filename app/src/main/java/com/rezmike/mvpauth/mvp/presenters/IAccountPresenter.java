package com.rezmike.mvpauth.mvp.presenters;

public interface IAccountPresenter {

    void clickOnAddAddress();

    void switchViewState();

    void switchSettings();

    void takePhoto();

    void chooseGallery();
    void chooseCamera();

    void editAddress(String addressId);
    void deleteAddress(String addressId);
}
