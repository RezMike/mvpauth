package com.rezmike.mvpauth.mvp.presenters;

public interface IProductPresenter {

    void clickOnPlus();
    void clickOnMinus();
    void clickOnFavorite();
    void clickOnShowMore();
}
