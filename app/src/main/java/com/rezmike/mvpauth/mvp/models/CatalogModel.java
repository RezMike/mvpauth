package com.rezmike.mvpauth.mvp.models;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;

import rx.Observable;

public class CatalogModel extends AbstractModel {

    public CatalogModel() {

    }

    public boolean isUserAuth() {
        return mDataManager.isUserAuth();
    }

    //region ======================== Products ========================

    public Observable<ProductRealm> getProductObs() {
        Observable<ProductRealm> disk = fromDisk();
        Observable<ProductRealm> network = fromNetwork();

        return Observable.mergeDelayError(disk, network)
                .distinct(ProductRealm::getId);
    }

    @RxLogObservable
    private Observable<ProductRealm> fromDisk() {
        return mDataManager.getProductsObsFromRealm();
    }

    @RxLogObservable
    private Observable<ProductRealm> fromNetwork() {
        return mDataManager.getProductsObsFromNetwork();
    }

    //endregion
}
