package com.rezmike.mvpauth.mvp.views;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.rezmike.mvpauth.ui.other.MenuItemHolder;

import java.util.List;

public interface IFabView {
    void setFabVisable(boolean visable);
    void setFabImage(int imageResId);
    void setFabColor(int colorResId);
    void setFabClickListener(View.OnClickListener listener);
}
