package com.rezmike.mvpauth.mvp.models;

import android.support.annotation.VisibleForTesting;

import com.birbit.android.jobqueue.JobManager;
import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.data.network.req.UserLoginReq;
import com.rezmike.mvpauth.data.network.res.UserRes;
import com.rezmike.mvpauth.data.storage.dto.FacebookDataDto;
import com.rezmike.mvpauth.data.storage.dto.VkDataDto;
import com.twitter.sdk.android.core.TwitterSession;

import rx.Observable;

import static android.support.annotation.VisibleForTesting.NONE;

public class AuthModel extends AbstractModel {

    public AuthModel() {
    }

    @VisibleForTesting(otherwise = NONE)
    public AuthModel(DataManager dataManager, JobManager jobManager) {
        super(dataManager, jobManager);
    }

    public String getUserName() {
        return mDataManager.getUserName();
    }

    public boolean isUserAuth() {
        return mDataManager.isUserAuth();
    }

    public void startUpdateLocalData() {
        mDataManager.startUpdateLocalDataWithTimer();
    }

    public Observable<UserRes> loginUser(String email, String password) {
        return mDataManager.loginUser(new UserLoginReq(email, password));
    }

    public Observable<UserRes> loginVk(String accessToken, String userId, String email) {
        return mDataManager.loginVk(new VkDataDto(accessToken, userId, email));
    }

    public Observable<UserRes> loginFacebook(String accessToken, String userId) {
        return mDataManager.loginFacebook(new FacebookDataDto(accessToken, userId));
    }

    public Observable<UserRes> loginTwitter(TwitterSession session) {
        return mDataManager.loginTwitter(session);
    }
}
