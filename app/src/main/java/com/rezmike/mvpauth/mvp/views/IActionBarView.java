package com.rezmike.mvpauth.mvp.views;

import android.support.v4.view.ViewPager;

import com.rezmike.mvpauth.ui.other.MenuItemHolder;

import java.util.List;

public interface IActionBarView {
    void setActionBarTitle(CharSequence title);
    void setActionBarVisable(boolean visable);
    void setBackArrow(boolean enabled);
    void setMenuItems(List<MenuItemHolder> items);
    void setTabLayout(ViewPager pager);
    void removeTabLayout();
}
