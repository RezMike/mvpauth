package com.rezmike.mvpauth.mvp.models;

import com.rezmike.mvpauth.data.storage.realm.CommentRealm;
import com.rezmike.mvpauth.jobs.SendMessageJob;

public class DetailModel extends AbstractModel {

    public void sendComment(String id, CommentRealm comment) {
        SendMessageJob job = new SendMessageJob(id, comment);
        mJobManager.addJobInBackground(job);
    }
}
