package com.rezmike.mvpauth.mvp.views;

import com.rezmike.mvpauth.data.storage.realm.AddressRealm;

public interface IAddressView extends IView {
    void showInputError();

    AddressRealm getUserAddress();
}
