package com.rezmike.mvpauth.mvp.views;

import android.support.v4.view.ViewPager;

import com.rezmike.mvpauth.ui.other.MenuItemHolder;

public interface IActionBarBuilder {

    IActionBarBuilder setActionBarTitle(CharSequence title);

    IActionBarBuilder setBackArrow(boolean enabled);

    IActionBarBuilder setVisable(boolean visable);

    IActionBarBuilder addAction(MenuItemHolder menuItem);

    IActionBarBuilder setTab(ViewPager pager);

    void build();
}
