package com.rezmike.mvpauth.mvp.models;

import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.data.storage.dto.UserSettingsDto;
import com.rezmike.mvpauth.data.storage.realm.AddressRealm;
import com.rezmike.mvpauth.jobs.UploadAvatarJob;

import rx.Observable;
import rx.subjects.BehaviorSubject;

public class AccountModel extends AbstractModel {

    private BehaviorSubject<ProfileInfoDto> mProfileInfoSub = BehaviorSubject.create();

    public AccountModel() {
        mProfileInfoSub.onNext(getProfileInfo());
    }

    //region ======================== Addresses ========================

    public Observable<AddressRealm> getAddressObs() {
        return mDataManager.getAddressesObsFromRealm();
    }

    public AddressRealm getAddress(String addressId) {
        return mDataManager.getAddress(addressId);
    }

    public void updateOrInsertAddress(AddressRealm address) {
        mDataManager.updateOrInsertAddress(address);
    }

    public void deleteAddress(String addressId) {
        mDataManager.deleteAddress(addressId);
    }

    //endregion

    //region ======================== Settings ========================

    public Observable<UserSettingsDto> getUserSettingsObs() {
        return Observable.just(getUserSettings());
    }

    private UserSettingsDto getUserSettings() {
        return mDataManager.getUserSettings();
    }

    public void saveSettings(UserSettingsDto settings) {
        mDataManager.saveSettings(settings);
    }

    //endregion

    //region ======================== ProfileInfo ========================

    public Observable<ProfileInfoDto> getProfileInfoSub() {
        return mProfileInfoSub;
    }

    private ProfileInfoDto getProfileInfo() {
        return mDataManager.getProfileInfo();
    }

    public void saveProfileInfo(ProfileInfoDto profileInfo) {
        mDataManager.saveProfileInfo(profileInfo);
        mProfileInfoSub.onNext(profileInfo);

        String uriAvatar = profileInfo.getAvatar();
        if (!uriAvatar.equals("null") && !uriAvatar.contains("http")) {
            uploadAvatarOnServer(uriAvatar);
        }
    }

    private void uploadAvatarOnServer(String imageUri) {
        mJobManager.addJobInBackground(new UploadAvatarJob(imageUri));
    }

    public void updateUserInfo() {
        mProfileInfoSub.onNext(getProfileInfo());
    }

    //endregion
}
