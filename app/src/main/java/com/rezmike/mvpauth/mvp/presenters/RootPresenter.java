package com.rezmike.mvpauth.mvp.presenters;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.ActivityResultDto;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.models.AccountModel;
import com.rezmike.mvpauth.mvp.views.IActionBarBuilder;
import com.rezmike.mvpauth.mvp.views.IActionBarView;
import com.rezmike.mvpauth.mvp.views.IFabBuilder;
import com.rezmike.mvpauth.mvp.views.IFabView;
import com.rezmike.mvpauth.mvp.views.IRootView;
import com.rezmike.mvpauth.ui.activities.RootActivity;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;
import com.rezmike.mvpauth.utils.ConstantManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mortar.MortarScope;
import mortar.Presenter;
import mortar.bundler.BundleService;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

public class RootPresenter extends Presenter<IRootView> {

    @Inject
    AccountModel mAccountModel;

    private PublishSubject<ActivityResultDto> mActivityResultDtoObs = PublishSubject.create();
    private Subscription mProfileInfoSub;

    public RootPresenter() {
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        ((RootActivity.RootComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected BundleService extractBundleService(IRootView view) {
        return BundleService.getBundleService((RootActivity) view);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);

        mProfileInfoSub = subscribeOnUserInfoObs();
    }

    @Override
    public void dropView(IRootView view) {
        if (mProfileInfoSub != null) {
            mProfileInfoSub.unsubscribe();
        }
        super.dropView(view);
    }

    private Subscription subscribeOnUserInfoObs() {
        return mAccountModel.getProfileInfoSub()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ProfileInfoSubscriber());
    }

    @Nullable
    public IRootView getRootView() {
        return getView();
    }

    public IActionBarBuilder newActionBarBuilder() {
        return new ActionBarBuilder();
    }

    public IFabBuilder newFabBuilder() {
        return new FabBuilder();
    }

    public void updateUserInfo() {
        mAccountModel.updateUserInfo();
    }

    @RxLogSubscriber
    private class ProfileInfoSubscriber extends Subscriber<ProfileInfoDto> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if (getView() != null) {
                getView().showError(e);
            }
        }

        @Override
        public void onNext(ProfileInfoDto profileInfoDto) {
            if (getView() != null) {
                getView().initDrawer(profileInfoDto);
            }
        }
    }

    public PublishSubject<ActivityResultDto> getActivityResultSubject() {
        return mActivityResultDtoObs;
    }

    public boolean checkPermissionAndRequestIfNotGranted(String[] permissions, int requestCode) {
        if (getView() == null) return false;
        boolean allGranted = true;
        allGranted = getView().isAllGranted(permissions, allGranted);

        if (!allGranted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ((RootActivity) getView()).requestPermissions(permissions, requestCode);
            }
        }
        return allGranted;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        mActivityResultDtoObs.onNext(new ActivityResultDto(requestCode, resultCode, intent));
    }

    public void onRequestPermissionResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantRes) {
        if (getView() != null && requestCode == ConstantManager.REQUEST_PERMISSION_CAMERA && grantRes.length == 2) {
            if (grantRes[0] == PackageManager.PERMISSION_DENIED) {
                getView().showMessage(R.string.photo_upload_need_camera_permission);
            }
            if (grantRes[1] == PackageManager.PERMISSION_DENIED) {
                getView().showMessage(R.string.photo_upload_need_write_permission);
            }
        }
    }

    public class ActionBarBuilder implements IActionBarBuilder {
        private final int DEFAULT_MODE = 0;
        private final int TAB_MODE = 1;

        private boolean isGoBack = false;
        private boolean isVisable = true;
        private CharSequence title = "";
        private List<MenuItemHolder> items = new ArrayList<>();
        private ViewPager pager;
        private int toolbarMode = DEFAULT_MODE;

        @Override
        public IActionBarBuilder setActionBarTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        @Override
        public IActionBarBuilder setBackArrow(boolean enabled) {
            this.isGoBack = enabled;
            return this;
        }

        @Override
        public IActionBarBuilder setVisable(boolean visable) {
            this.isVisable = visable;
            return this;
        }

        @Override
        public IActionBarBuilder addAction(MenuItemHolder menuItem) {
            this.items.add(menuItem);
            return this;
        }

        @Override
        public IActionBarBuilder setTab(ViewPager pager) {
            this.toolbarMode = TAB_MODE;
            this.pager = pager;
            return this;
        }

        @Override
        public void build() {
            if (getRootView() == null) return;
            IActionBarView view = (IActionBarView) getRootView();
            view.setActionBarVisable(isVisable);
            view.setActionBarTitle(title);
            view.setBackArrow(isGoBack);
            view.setMenuItems(items);
            if (toolbarMode == TAB_MODE) {
                view.removeTabLayout();
                view.setTabLayout(pager);
            } else {
                view.removeTabLayout();
            }
        }
    }

    public class FabBuilder implements IFabBuilder {
        private boolean isVisable = true;
        private int imageResId;
        private int colorResId;
        private View.OnClickListener listener;

        @Override
        public IFabBuilder setVisable(boolean visable) {
            isVisable = visable;
            return this;
        }

        @Override
        public IFabBuilder setImageResId(int imageResId) {
            this.imageResId = imageResId;
            return this;
        }

        @Override
        public IFabBuilder setColorResId(int colorResId) {
            this.colorResId = colorResId;
            return this;
        }

        @Override
        public IFabBuilder setListener(View.OnClickListener listener) {
            this.listener = listener;
            return this;
        }

        @Override
        public void build() {
            if (getRootView() == null) return;
            IFabView view = (IFabView) getRootView();
            view.setFabVisable(isVisable);
            view.setFabImage(imageResId);
            view.setFabColor(colorResId);
            if (isVisable) {
                view.setFabClickListener(listener);
            } else {
                view.setFabClickListener(null);
            }
        }
    }

    public void setFabImage(int imageResId) {
        RootActivity activity = (RootActivity) getView();
        activity.setFabImage(imageResId);
    }
}
