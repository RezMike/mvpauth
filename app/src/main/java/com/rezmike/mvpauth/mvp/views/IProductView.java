package com.rezmike.mvpauth.mvp.views;

import com.rezmike.mvpauth.data.storage.dto.ProductDto;

public interface IProductView extends IView {

    void showProductView(ProductDto product);

    void updateProductCountView(ProductDto product);
}
