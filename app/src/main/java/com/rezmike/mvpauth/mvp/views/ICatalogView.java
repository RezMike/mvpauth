package com.rezmike.mvpauth.mvp.views;

public interface ICatalogView extends IView {

    void showCatalogView();
    void showCartIndicator();

    void updateProductCounter();
}
