package com.rezmike.mvpauth.mvp.views;

public interface IAuthView extends IView {

    void showLoginBtn();
    void hideLoginBtn();

    String getUserEmail();
    String getUserPassword();

    boolean isIdle();

    void setLoginEnabled(boolean enabled);

    void setEmailError(int errorStringId);
    void setPasswordError(int errorStringId);

    void animateVkBtn();
    void animateFacebookBtn();
    void animateTwitterBtn();
}
