package com.rezmike.mvpauth.flow;

import android.util.Log;

import com.rezmike.mvpauth.mortar.ScreenScoper;

import flow.ClassKey;

import static android.content.ContentValues.TAG;

public abstract class AbstractScreen<T> extends ClassKey {

    public String getScopeName() {
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    public void unregisterScope() {
        Log.d(TAG, "unregisterScope: " + getScopeName());
        ScreenScoper.destroyScreenScope(getScopeName());
    }

    public int getLayoutResId() {
        int layout = 0;
        Screen screen;
        screen = getClass().getAnnotation(Screen.class);
        if (screen == null) {
            throw new IllegalStateException("@Screen annotation is missing on screen " + getScopeName());
        } else {
            layout = screen.value();
        }
        return layout;
    }
}
