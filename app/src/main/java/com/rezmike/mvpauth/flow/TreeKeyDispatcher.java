package com.rezmike.mvpauth.flow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.mortar.ScreenScoper;
import com.rezmike.mvpauth.ui.screens.auth.AuthScreen;
import com.rezmike.mvpauth.utils.ViewHelper;

import java.util.Collections;
import java.util.Map;

import flow.Direction;
import flow.Dispatcher;
import flow.KeyChanger;
import flow.MultiKey;
import flow.State;
import flow.Traversal;
import flow.TraversalCallback;
import flow.TreeKey;

public class TreeKeyDispatcher extends KeyChanger implements Dispatcher {

    private Activity mActivity;
    private Object inKey;
    @Nullable
    private Object outKey;
    private FrameLayout mRootFrame;

    public TreeKeyDispatcher(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void dispatch(Traversal traversal, TraversalCallback callback) {
        Map<Object, Context> contexts;
        State inState = traversal.getState(traversal.destination.top());
        inKey = inState.getKey();
        State outState = traversal.origin == null ? null : traversal.getState(traversal.origin.top());
        outKey = outState == null ? null : outState.getKey();

        mRootFrame = (FrameLayout) mActivity.findViewById(R.id.root_frame);

        if (inKey.equals(outKey)) {
            callback.onTraversalCompleted();
            return;
        }

        if (inKey instanceof MultiKey) {
            // implement MultiKey case
        }

        Context flowContext = traversal.createContext(inKey, mActivity);
        Context mortarContext = ScreenScoper.getScreenScope((AbstractScreen) inKey).createContext(flowContext);
        contexts = Collections.singletonMap(inKey, mortarContext);
        changeKey(outState, inState, traversal.direction, contexts, callback);
    }

    @Override
    public void changeKey(@Nullable State outgoingState, State incomingState, Direction direction,
                          Map<Object, Context> incomingContexts, TraversalCallback callback) {

        Context context = incomingContexts.get(inKey);

        //save prev view

        if (outgoingState != null) {
            outgoingState.save(mRootFrame.getChildAt(0));
        }

        //create new view

        Screen screen = inKey.getClass().getAnnotation(Screen.class);
        if (screen == null) {
            throw new IllegalStateException("@Screen annotation is missing on screen " +
                    ((AbstractScreen) inKey).getScopeName());
        }
        int layout = screen.value();

        View newView = LayoutInflater.from(context).inflate(layout, mRootFrame, false);
        View oldView = mRootFrame.getChildAt(0);

        //restore state to new view
        incomingState.restore(newView);

        mRootFrame.addView(newView);

        if (oldView == null) {
            callback.onTraversalCompleted();
            return;
        }

        if (outKey instanceof AuthScreen && ((AuthScreen) outKey).isSplashTarget()
                || inKey instanceof AuthScreen && ((AuthScreen) inKey).isSplashTarget()) {
            mRootFrame.removeView(oldView);
            callback.onTraversalCompleted();
            return;
        }

        ViewHelper.waitForMeasure(newView, (view, width, height) ->
                runAnimation(mRootFrame, oldView, newView, direction, () -> {
                    if (outKey != null && !(inKey instanceof TreeKey)) {
                        ((AbstractScreen) outKey).unregisterScope();
                    }
                    callback.onTraversalCompleted();
                }));
    }

    private void runAnimation(FrameLayout container, View from, View to, Direction direction, TraversalCallback callback) {
        Animator animator = createAnimation(from, to, direction);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (from != null) {
                    container.removeView(from);
                }
                callback.onTraversalCompleted();
            }
        });

        animator.setInterpolator(new FastOutLinearInInterpolator());
        animator.start();
    }

    @NonNull
    private Animator createAnimation(@Nullable View from, View to, Direction direction) {
        boolean backward = direction == Direction.BACKWARD;

        AnimatorSet set = new AnimatorSet();

        if (from != null) {
            int fromTranslation = backward ? from.getWidth() : -from.getWidth();
            ObjectAnimator outAnimation = ObjectAnimator.ofFloat(from, "translationX", fromTranslation);
            set.play(outAnimation);
        }

        int toTranslation = backward ? -to.getWidth() : to.getWidth();
        ObjectAnimator toAnimation = ObjectAnimator.ofFloat(to, "translationX", toTranslation, 0);
        set.play(toAnimation);

        return set;
    }
}
