package com.rezmike.mvpauth.services;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.data.storage.realm.NotificationRealm;
import com.rezmike.mvpauth.utils.NotificationHelper;

import java.util.Date;

public class PushService extends FirebaseMessagingService {

    private static final String TAG = "PushService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "onMessageReceived: ");

        String id = remoteMessage.getMessageId();
        String type = remoteMessage.getData().get("type");
        String meta = remoteMessage.getData().get("meta");
        String title = remoteMessage.getNotification().getTitle();
        String content = remoteMessage.getNotification().getBody();
        Date date = new Date(remoteMessage.getSentTime());

        NotificationRealm notification = new NotificationRealm(id, type, meta, title, content, date);
        DataManager.getInstance().saveNotification(notification);

        NotificationHelper.createNotification(remoteMessage.getNotification(), type, meta);
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }
}
