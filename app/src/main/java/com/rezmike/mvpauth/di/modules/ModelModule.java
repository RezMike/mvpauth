package com.rezmike.mvpauth.di.modules;

import com.rezmike.mvpauth.data.managers.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelModule extends FlavorModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return DataManager.getInstance();
    }
}
