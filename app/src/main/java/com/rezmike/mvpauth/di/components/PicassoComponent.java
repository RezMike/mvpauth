package com.rezmike.mvpauth.di.components;

import com.rezmike.mvpauth.di.modules.PicassoCacheModule;
import com.rezmike.mvpauth.di.scopes.RootScope;
import com.squareup.picasso.Picasso;

import dagger.Component;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
