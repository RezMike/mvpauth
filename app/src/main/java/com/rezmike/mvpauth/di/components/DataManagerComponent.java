package com.rezmike.mvpauth.di.components;

import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.di.modules.LocalModule;
import com.rezmike.mvpauth.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
