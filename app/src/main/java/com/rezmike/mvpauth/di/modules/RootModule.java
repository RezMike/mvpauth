package com.rezmike.mvpauth.di.modules;

import com.rezmike.mvpauth.di.scopes.RootScope;
import com.rezmike.mvpauth.mvp.models.AccountModel;
import com.rezmike.mvpauth.mvp.presenters.RootPresenter;

import dagger.Provides;

@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    public RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }

    @Provides
    @RootScope
    public AccountModel provideAccountModel() {
        return new AccountModel();
    }
}
