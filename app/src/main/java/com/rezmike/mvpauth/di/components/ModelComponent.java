package com.rezmike.mvpauth.di.components;

import com.rezmike.mvpauth.di.modules.ModelModule;
import com.rezmike.mvpauth.mvp.models.AbstractModel;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
