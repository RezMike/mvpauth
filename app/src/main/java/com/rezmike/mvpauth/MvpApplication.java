package com.rezmike.mvpauth;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.components.AppComponent;
import com.rezmike.mvpauth.di.components.DaggerAppComponent;
import com.rezmike.mvpauth.di.modules.AppModule;
import com.rezmike.mvpauth.di.modules.PicassoCacheModule;
import com.rezmike.mvpauth.di.modules.RootModule;
import com.rezmike.mvpauth.mortar.ScreenScoper;
import com.rezmike.mvpauth.services.PushService;
import com.rezmike.mvpauth.ui.activities.DaggerRootActivity_RootComponent;
import com.rezmike.mvpauth.ui.activities.RootActivity;
import com.vk.sdk.VKSdk;

import io.realm.Realm;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class MvpApplication extends Application {

    private static AppComponent sAppComponent;
    private MortarScope mRootScope;
    private MortarScope mRootActivityScope;
    private RootActivity.RootComponent mRootActivityRootComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        VKSdk.initialize(this);

        createScopes();

        Intent pushIntent = new Intent(this, PushService.class);
        startService(pushIntent);
    }

    @Override
    public Object getSystemService(String name) {
        if (mRootScope != null) {
            return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
        } else {
            return super.getSystemService(name);
        }
    }

    private void createScopes() {
        createAppComponent();
        createRootActivityComponent();

        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");

        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);
    }

    private void createAppComponent() {
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    private void createRootActivityComponent() {
        mRootActivityRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(sAppComponent)
                .rootModule(new RootModule())
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static Context getContext() {
        return sAppComponent.getContext();
    }
}
