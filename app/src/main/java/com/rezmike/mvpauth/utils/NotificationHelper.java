package com.rezmike.mvpauth.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.firebase.messaging.RemoteMessage;
import com.rezmike.mvpauth.MvpApplication;
import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.ui.activities.RootActivity;

public class NotificationHelper {

    public static void createSimpleNotification(String title, String content) {
        final Context context = MvpApplication.getContext();
        Intent contentIntent = new Intent(context, RootActivity.class);

        PendingIntent resultIntent = PendingIntent.getActivity(context, ConstantManager.PUSH_REQ_CODE,
                contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(resultIntent)
                .setSmallIcon(R.drawable.firebase_icon)
                .setAutoCancel(true)
                .setColor(context.getResources().getColor(R.color.color_accent))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(ConstantManager.NOTIFICATION_MANAGER_ID, builder.build());
    }

    public static void createNotification(RemoteMessage.Notification notification, @Nullable String type, @Nullable String meta) {
        final Context context = MvpApplication.getContext();
        Intent contentIntent = new Intent(context, RootActivity.class);

        String title = notification.getTitle();
        if (title == null || title.isEmpty()) {
            title = context.getString(R.string.app_name);
        }
        String content = notification.getBody();
        PendingIntent resultIntent = PendingIntent.getActivity(context, ConstantManager.PUSH_REQ_CODE,
                contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(resultIntent)
                .setSmallIcon(R.drawable.firebase_icon)
                .setAutoCancel(true)
                .setColor(context.getResources().getColor(R.color.color_accent))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        if (type != null && !type.isEmpty()) {
            if (TextUtils.equals(type, ConstantManager.NOTIFICATION_ORDER_TYPE)) {
                // TODO: 05.05.2017 add remove order from server logic (in meta order id)
                builder.addAction(R.drawable.ic_close_black_24dp, context.getString(R.string.notification_cancel_order), resultIntent)
                        .addAction(R.drawable.ic_visibility_black_24dp, context.getString(R.string.notification_status), resultIntent);
            } else if (TextUtils.equals(type, ConstantManager.NOTIFICATION_PROMO_TYPE)) {
                // TODO: 05.05.2017 add meta from notification in intent (in meta http link)
                builder.addAction(R.drawable.ic_visibility_black_24dp, context.getString(R.string.notification_more), resultIntent);
            }
        }

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(ConstantManager.NOTIFICATION_MANAGER_ID, builder.build());
    }
}
