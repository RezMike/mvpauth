package com.rezmike.mvpauth.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.rezmike.mvpauth.MvpApplication;

import rx.Observable;

public class NetworkStatusChecker {

    public static Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) MvpApplication.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static Observable<Boolean> isInternetAvailable() {
        return Observable.just(isNetworkAvailable());
    }
}
