package com.rezmike.mvpauth.ui.screens.product_details.comments;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.realm.CommentRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;

import butterknife.BindView;

public class CommentsView extends AbstractView<CommentsScreen.CommentsPresenter> {

    private CommentsAdapter mAdapter;

    @BindView(R.id.comments_list)
    RecyclerView mCommentsList;

    public CommentsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<CommentsScreen.Component>getDaggerComponent(context).inject(this);
    }

    void initView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mCommentsList.setLayoutManager(layoutManager);
        mAdapter = new CommentsAdapter();
        mCommentsList.setAdapter(mAdapter);
    }

    public void showAddCommentDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());

        View dialogView = inflater.inflate(R.layout.dialog_comment, null);

        AppCompatRatingBar ratingBar = (AppCompatRatingBar) dialogView.findViewById(R.id.comment_rb);
        EditText commentEt = (EditText) dialogView.findViewById(R.id.comment_et);

        dialogBuilder.setTitle(R.string.comment_dialog_title)
                .setView(dialogView)
                .setPositiveButton(R.string.comment_dialog_add_comment, (dialog, which) -> {
                    CommentRealm comment = new CommentRealm(ratingBar.getRating(), commentEt.getText().toString());
                    mPresenter.addComment(comment);
                })
                .setNegativeButton(R.string.comment_dialog_cancel, (dialog, which) -> dialog.cancel())
                .show();

    }

    public CommentsAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
}
