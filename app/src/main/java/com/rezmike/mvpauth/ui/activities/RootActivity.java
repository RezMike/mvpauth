package com.rezmike.mvpauth.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.rezmike.mvpauth.BuildConfig;
import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.data.storage.realm.NotificationRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.components.AppComponent;
import com.rezmike.mvpauth.di.modules.PicassoCacheModule;
import com.rezmike.mvpauth.di.modules.RootModule;
import com.rezmike.mvpauth.di.scopes.RootScope;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.flow.TreeKeyDispatcher;
import com.rezmike.mvpauth.mvp.models.AccountModel;
import com.rezmike.mvpauth.mvp.presenters.RootPresenter;
import com.rezmike.mvpauth.mvp.views.IActionBarView;
import com.rezmike.mvpauth.mvp.views.IFabView;
import com.rezmike.mvpauth.mvp.views.IRootView;
import com.rezmike.mvpauth.mvp.views.IView;
import com.rezmike.mvpauth.ui.custom_views.CircleImageView;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;
import com.rezmike.mvpauth.ui.screens.account.AccountScreen;
import com.rezmike.mvpauth.ui.screens.auth.AuthScreen;
import com.rezmike.mvpauth.ui.screens.catalog.CatalogScreen;
import com.rezmike.mvpauth.ui.screens.notifications.NotificationsScreen;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class RootActivity extends BaseActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener,
        IActionBarView, IFabView {

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    Picasso mPicasso;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.appbar_layout)
    AppBarLayout mAppbar;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;
    @BindView(R.id.fab)
    FloatingActionButton mFab;
    //@BindView(R.id.basket_indicator)
    //ImageView mBasketIndicator;

    private ActionBarDrawerToggle mToggle;
    private ActionBar mActionBar;
    private List<MenuItemHolder> mActionBarMenuItems;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen(AuthScreen.SPLASH_TARGET))
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    //region ======================== Activity Lifecycle ========================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);

        DaggerService.<RootComponent>getDaggerComponent(this).inject(this);

        setupToolbar();
        mRootPresenter.takeView(this);
        handlePush();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView(this);
        super.onDestroy();
    }

    //endregion

    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    private void setupToolbar() {
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void handlePush() {
        Bundle ex = getIntent().getExtras();
        if (ex != null) {
            String id = ex.getString("google.message_id");
            Date date = new Date(ex.getLong("google.sent_time"));
            String title = ex.getString("title");
            String content = ex.getString("content");
            String meta = ex.getString("meta");
            String type = ex.getString("type");

            NotificationRealm notification = new NotificationRealm(id, type, meta, title, content, date);
            DataManager.getInstance().saveNotification(notification);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AbstractScreen nextScreen = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                nextScreen = new AccountScreen();
                break;
            case R.id.nav_catalog:
                nextScreen = new CatalogScreen();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notifications:
                nextScreen = new NotificationsScreen();
                break;
        }
        if (nextScreen != null) {
            Flow.get(this).set(nextScreen);
        }
        item.setChecked(true);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mRootPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mRootPresenter.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.exit_dialog_title)
                    .setPositiveButton(R.string.exit_dialog_yes, (dialog, which) -> finish())
                    .setNegativeButton(R.string.exit_dialog_no, null)
                    .create();
            alertDialog.show();
        }
    }

    //region ======================== Events ========================

    //@OnClick(R.id.basket)
    void basketClick() {
        // TODO: 31.10.2016 show cart fragment
    }

    //endregion

    //region ======================== IRootView ========================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(int stringResId) {
        showMessage(getString(stringResId));
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(R.string.sorry_something_wrong);
            // TODO: 23.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public void initDrawer(ProfileInfoDto profileInfo) {
        View headerLayout = mNavigationView.getHeaderView(0);
        TextView userNameTv = (TextView) headerLayout.findViewById(R.id.user_name);
        CircleImageView avatarImg = (CircleImageView) headerLayout.findViewById(R.id.circle_avatar);

        userNameTv.setText(profileInfo.getName());

        mPicasso.load(profileInfo.getAvatar())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.account_avatar)
                .into(avatarImg);
    }

    @Override
    public boolean isAllGranted(String[] permissions, boolean allGranted) {
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission(this, permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        return allGranted;
    }

    @Override
    public void showBasketIndicator() {
        //mBasketIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void changeMenuItem(int position, MenuItemHolder newItemHolder) {
        mActionBarMenuItems.set(position, newItemHolder);
        supportInvalidateOptionsMenu();
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

    //region ======================== IActionBarView ========================

    @Override
    public void setActionBarTitle(CharSequence title) {
        mToolbar.setTitle(title);
    }

    @Override
    public void setActionBarVisable(boolean visable) {
        if (visable) {
            showToolbar();
        } else {
            hideToolbar();
        }
    }

    private void showToolbar() {
        if (mActionBar != null) {
            mActionBar.show();
        }
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mRootFrame.getLayoutParams();
        layoutParams.setMargins(0, actionBarHeight, 0, 0);
        mRootFrame.setLayoutParams(layoutParams);
    }

    private void hideToolbar() {
        if (mActionBar != null) {
            mActionBar.hide();
        }
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mRootFrame.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, 0);
        mRootFrame.setLayoutParams(layoutParams);
        mRootFrame.setFitsSystemWindows(true);
    }

    @Override
    public void setBackArrow(boolean enabled) {
        if (mToggle == null || mActionBar == null) return;

        if (enabled) {
            mToggle.setDrawerIndicatorEnabled(false);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            if (mToggle.getToolbarNavigationClickListener() == null) {
                mToggle.setToolbarNavigationClickListener(v -> onBackPressed());
            }
        } else {
            mActionBar.setDisplayHomeAsUpEnabled(false);
            mToggle.setDrawerIndicatorEnabled(true);
            mToggle.setToolbarNavigationClickListener(null);
        }

        mDrawerLayout.setDrawerLockMode(enabled ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
        mToggle.syncState();
    }

    @Override
    public void setMenuItems(List<MenuItemHolder> items) {
        mActionBarMenuItems = items;
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mActionBarMenuItems != null && !mActionBarMenuItems.isEmpty()) {
            for (MenuItemHolder menuItem : mActionBarMenuItems) {
                MenuItem item = menu.add(menuItem.getTitle());
                item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                        .setIcon(menuItem.getIconResId())
                        .setOnMenuItemClickListener(menuItem.getListener());
            }
        } else {
            menu.clear();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTabLayout(ViewPager pager) {
        TabLayout tabView = new TabLayout(this);
        tabView.setupWithViewPager(pager);
        mAppbar.addView(tabView);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
    }

    @Override
    public void removeTabLayout() {
        View tabView = mAppbar.getChildAt(1);
        if (tabView != null && tabView instanceof TabLayout) {
            mAppbar.removeView(tabView);
        }
    }

    //endregion

    //region ======================== IFabView ========================

    @Override
    public void setFabVisable(boolean visable) {
        if (visable) {
            mFab.setVisibility(View.VISIBLE);
        } else {
            mFab.setVisibility(View.GONE);
        }
    }

    @Override
    public void setFabImage(int imageResId) {
        if (imageResId == 0) return;
        mFab.setImageResource(imageResId);
    }

    @Override
    public void setFabColor(int colorResId) {
        if (colorResId == 0) return;
        mFab.setBackgroundTintList(getResources().getColorStateList(colorResId));
    }

    @Override
    public void setFabClickListener(View.OnClickListener listener) {
        mFab.setOnClickListener(listener);
    }

    //endregion

    //region ======================== DI ========================

    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {
        void inject(RootActivity activity);

        void inject(RootPresenter presenter);

        AccountModel getAccountModel();

        RootPresenter getRootPresenter();

        Picasso getPicasso();
    }

    //endregion
}
