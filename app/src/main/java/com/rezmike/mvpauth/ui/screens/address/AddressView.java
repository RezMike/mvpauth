package com.rezmike.mvpauth.ui.screens.address;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.AddressDto;
import com.rezmike.mvpauth.data.storage.realm.AddressRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;
import com.rezmike.mvpauth.mvp.views.IAddressView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class AddressView extends AbstractView<AddressScreen.AddressPresenter> implements IAddressView {

    @Inject
    AddressScreen.AddressPresenter mPresenter;

    @BindView(R.id.address_name_et)
    EditText mAddressNameEt;
    @BindView(R.id.street_et)
    EditText mNumberStreetEt;
    @BindView(R.id.house_et)
    EditText mNumberHouseEt;
    @BindView(R.id.apartment_et)
    EditText mNumberApartmentEt;
    @BindView(R.id.floor_et)
    EditText mNumberFloorEt;
    @BindView(R.id.comment_et)
    EditText mCommentEt;
    @BindView(R.id.add_address_btn)
    Button mAddAddressBtn;

    private String mAddressId;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
    }

    //region ======================== Events ========================

    @OnClick(R.id.add_address_btn)
    void addAddressClick() {
        mPresenter.clickOnAddAddress();
    }

    //endregion

    //region ======================== IAddressView ========================

    public void showAddressView(@Nullable AddressDto address) {
        if (address == null) return;
        mAddressId = address.getId();
        mAddressNameEt.setText(address.getName());
        mNumberStreetEt.setText(String.valueOf(address.getStreet()));
        mNumberHouseEt.setText(String.valueOf(address.getHouse()));
        mNumberApartmentEt.setText(String.valueOf(address.getApartment()));
        mNumberFloorEt.setText(String.valueOf(address.getFloor()));
        mCommentEt.setText(address.getComment());
        mAddAddressBtn.setText(R.string.address_save);
    }

    @Override
    public void showInputError() {
        if (mPresenter.getRootView() == null) return;
        mPresenter.getRootView().showMessage(R.string.address_input_error);
    }

    @Override
    @Nullable
    public AddressRealm getUserAddress() {
        String destination = mAddressNameEt.getText().toString();
        String street = mNumberStreetEt.getText().toString();
        String house = mNumberHouseEt.getText().toString();
        String apartment = mNumberApartmentEt.getText().toString();
        String floor = mNumberFloorEt.getText().toString();
        String comment = mCommentEt.getText().toString();
        if (destination.isEmpty() || street.isEmpty() || house.isEmpty()
                || house.isEmpty() || apartment.isEmpty() || floor.isEmpty()) {
            showInputError();
            return null;
        }
        if (comment.isEmpty()) comment = "-";
        return new AddressRealm(
                mAddressId,
                destination,
                street,
                house,
                apartment,
                Integer.parseInt(floor),
                comment
        );
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

}
