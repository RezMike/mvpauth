package com.rezmike.mvpauth.ui.screens.product_details;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.scopes.DaggerScope;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.flow.Screen;
import com.rezmike.mvpauth.mvp.models.DetailModel;
import com.rezmike.mvpauth.mvp.presenters.AbstractPresenter;
import com.rezmike.mvpauth.mvp.presenters.RootPresenter;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;
import com.rezmike.mvpauth.ui.screens.catalog.CatalogScreen;
import com.rezmike.mvpauth.ui.screens.product_details.comments.CommentsView;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.TreeKey;
import io.realm.Realm;
import mortar.MortarScope;

@Screen(R.layout.screen_detail)
public class DetailScreen extends AbstractScreen<CatalogScreen.Component> implements TreeKey {

    private ProductRealm mProductRealm;

    public DetailScreen(ProductRealm product) {
        mProductRealm = product;
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerDetailScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new CatalogScreen();
    }

    //region ======================== DI ========================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(DetailScreen.class)
        DetailPresenter provideDetailPresenter() {
            return new DetailPresenter(mProductRealm);
        }

        @Provides
        @DaggerScope(DetailScreen.class)
        DetailModel provideDetailModel() {
            return new DetailModel();
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = DetailScreen.Module.class)
    @DaggerScope(DetailScreen.class)
    public interface Component {
        void inject(DetailPresenter presenter);

        void inject(DetailView view);

        RootPresenter getRootPresenter();

        Picasso getPicasso();

        DetailModel getDetailModel();
    }

    //endregion

    //region ======================== Presenter ========================

    public class DetailPresenter extends AbstractPresenter<DetailView, DetailModel> {

        private final ProductRealm mProduct;

        public DetailPresenter(ProductRealm product) {
            mProduct = product;
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setActionBarTitle(mProduct.getProductName())
                    .setBackArrow(true)
                    .addAction(new MenuItemHolder(
                            getView().getContext().getString(R.string.actionbar_menu_basket),
                            R.drawable.ic_shopping_basket_black_24dp,
                            item -> {
                                if (getRootView() != null)
                                    getRootView().showMessage(R.string.actionbar_go_to_basket);
                                return true;
                            }))
                    .setTab(getView().getViewPager())
                    .build();
        }

        @Override
        protected void initFab() {
            ViewPager viewPager = getView().getViewPager();
            switch (viewPager.getCurrentItem()) {
                case 0:
                    mRootPresenter.newFabBuilder()
                            .setVisable(true)
                            .setImageResId(R.drawable.ic_favorite_white_alpha_24dp)
                            .setColorResId(R.color.color_accent)
                            .setListener(v -> clickOnFavorite())
                            .build();
                    updateFavoriteFab();
                    break;
                case 1:
                    mRootPresenter.newFabBuilder()
                            .setVisable(true)
                            .setImageResId(R.drawable.ic_add_white_24dp)
                            .setColorResId(R.color.color_accent)
                            .setListener(v -> clickOnAddComment())
                            .build();
                    break;
            }
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView(mProduct);
        }

        private void clickOnFavorite() {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> mProduct.changeFavorite());
            realm.close();
            updateFavoriteFab();
        }

        private void clickOnAddComment() {
            View view = getView().getViewPager().getChildAt(1);
            if (view != null && view instanceof CommentsView) {
                ((CommentsView) view).showAddCommentDialog();
            }
        }

        private void updateFavoriteFab() {
            mRootPresenter.setFabImage(mProduct.isFavorite() ?
                    R.drawable.ic_favorite_white_24dp :
                    R.drawable.ic_favorite_white_alpha_24dp);
        }
    }

    //endregion
}
