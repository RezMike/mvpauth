package com.rezmike.mvpauth.ui.screens.product;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.ProductDto;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;
import com.rezmike.mvpauth.mvp.views.IProductView;
import com.rezmike.mvpauth.utils.ViewHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.ChangeImageTransform;
import com.transitionseverywhere.Explode;
import com.transitionseverywhere.SidePropagation;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ProductView extends AbstractView<ProductScreen.ProductPresenter> implements IProductView {

    @Inject
    Picasso mPicasso;

    @BindView(R.id.product_wrapper)
    LinearLayout mProductWrapper;
    @BindView(R.id.product_card)
    CardView mProductCard;
    @BindView(R.id.product_name_tv)
    TextView mProductNameTv;
    @BindView(R.id.product_description_tv)
    TextView mProductDescriptionTv;
    @BindView(R.id.product_img)
    ImageView mProductImg;
    @BindView(R.id.product_count_tv)
    TextView mProductCountTv;
    @BindView(R.id.product_price_tv)
    TextView mProductPriceTv;
    @BindView(R.id.favorite_btn)
    CheckBox mFavoriteBtn;
    @BindView(R.id.show_more_btn)
    Button mShowMoreBtn;

    private AnimatorSet mResultSet;
    private List<View> mChildrenList;
    private int mInImageHeight;
    private float mDen;
    private boolean isZoomed;

    public ProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mDen = ViewHelper.getDensity(context);
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<ProductScreen.Component>getDaggerComponent(context).inject(this);
    }

    //region ======================== Events ========================

    @OnClick(R.id.plus_btn)
    void plusBtnClick() {
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.minus_btn)
    void minusBtnClick() {
        mPresenter.clickOnMinus();
    }

    @OnClick(R.id.favorite_btn)
    void clickOnFavorite() {
        mPresenter.clickOnFavorite();
    }

    @OnClick(R.id.show_more_btn)
    void clickOnShowMore() {
        mPresenter.clickOnShowMore();
    }

    @OnClick(R.id.product_img)
    void zoomImage() {
        startZoomTransition();
    }

    //endregion

    //region ======================== Animation ========================

    public void startAddToCartAnimation() {
        final int cx = (mProductWrapper.getLeft() + mProductWrapper.getRight()) / 2;
        final int cy = (mProductWrapper.getTop() + mProductWrapper.getBottom()) / 2;
        final int radius = Math.max(mProductWrapper.getWidth(), mProductWrapper.getHeight());

        Animator hideCircleAnim;
        Animator showCircleAnim;
        Animator hideColorAnim = null;
        Animator showColorAnim = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            hideCircleAnim = ViewAnimationUtils.createCircularReveal(mProductWrapper, cx, cy, radius, 0);
            hideCircleAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mProductWrapper.setVisibility(INVISIBLE);
                }
            });
            showCircleAnim = ViewAnimationUtils.createCircularReveal(mProductWrapper, cx, cy, 0, radius);
            showCircleAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    mProductWrapper.setVisibility(VISIBLE);
                }
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ColorDrawable cdr = (ColorDrawable) mProductWrapper.getForeground();
                hideColorAnim = ObjectAnimator.ofArgb(cdr, "color", getResources().getColor(R.color.color_accent, null));
                showColorAnim = ObjectAnimator.ofArgb(cdr, "color", getResources().getColor(R.color.transparent, null));
            }
        } else {
            hideCircleAnim = ObjectAnimator.ofFloat(mProductWrapper, "alpha", 0);
            showCircleAnim = ObjectAnimator.ofFloat(mProductWrapper, "alpha", 1);
        }

        AnimatorSet hideSet = new AnimatorSet();
        AnimatorSet showSet = new AnimatorSet();

        addAnimatorTogetherSet(hideSet, hideCircleAnim, hideColorAnim);
        addAnimatorTogetherSet(showSet, showCircleAnim, showColorAnim);

        hideSet.setDuration(600);
        hideSet.setInterpolator(new FastOutSlowInInterpolator());

        showSet.setStartDelay(1000);
        showSet.setDuration(600);
        showSet.setInterpolator(new FastOutSlowInInterpolator());

        if (mResultSet != null && !mResultSet.isStarted() || mResultSet == null) {
            mResultSet = new AnimatorSet();
            mResultSet.playSequentially(hideSet, showSet);
            mResultSet.start();
        }
    }

    private void addAnimatorTogetherSet(AnimatorSet set, Animator... anims) {
        ArrayList<Animator> animatorList = new ArrayList<>();
        for (Animator animator : anims) {
            if (animator != null) {
                animatorList.add(animator);
            }
        }
        set.playTogether(animatorList);
    }

    private void startZoomTransition() {

        TransitionSet set = new TransitionSet();

        final Rect rect = new Rect(mProductImg.getLeft(), mProductImg.getTop(), mProductImg.getRight(), mProductImg.getBottom());

        Transition explode = new Explode();
        explode.setEpicenterCallback(new Transition.EpicenterCallback() {
            @Override
            public Rect onGetEpicenter(Transition transition) {
                return rect;
            }
        });

        SidePropagation prop = new SidePropagation();
        prop.setPropagationSpeed(3f);
        explode.setPropagation(prop);

        ChangeBounds changeBounds = new ChangeBounds();
        ChangeImageTransform imageTransform = new ChangeImageTransform();

        if (!isZoomed) {
            changeBounds.setStartDelay(100);
            imageTransform.setStartDelay(100);
        }

        set.addTransition(explode)
                .addTransition(changeBounds)
                .addTransition(imageTransform)
                .setDuration(600)
                .setInterpolator(new FastOutSlowInInterpolator());

        TransitionManager.beginDelayedTransition(mProductCard, set);

        if (mChildrenList == null) {
            mChildrenList = ViewHelper.getChildrenExcludeView(mProductWrapper, R.id.product_img);
        }

        ViewGroup.LayoutParams cardParams = mProductCard.getLayoutParams();
        cardParams.height = !isZoomed ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT;
        mProductCard.setLayoutParams(cardParams);

        ViewGroup.LayoutParams wrapParams = mProductWrapper.getLayoutParams();
        wrapParams.height = !isZoomed ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT;
        mProductWrapper.setLayoutParams(wrapParams);

        LinearLayout.LayoutParams imgParams;
        if (!isZoomed) {
            mInImageHeight = mProductImg.getHeight();
            imgParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mProductImg.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            imgParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mInImageHeight);
            int defMargin = (int) (16 * mDen);
            imgParams.setMargins(defMargin, 0, defMargin, 0);
            mProductImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        mProductImg.setLayoutParams(imgParams);

        if (!isZoomed) {
            for (View view : mChildrenList) {
                view.setVisibility(GONE);
            }
        } else {
            for (View view : mChildrenList) {
                view.setVisibility(VISIBLE);
            }
        }

        isZoomed = !isZoomed;
    }

    //endregion

    //region ======================== IProductView ========================

    @Override
    public void showProductView(final ProductDto product) {
        mProductNameTv.setText(product.getProductName());
        mProductDescriptionTv.setText(product.getDescription());
        mProductCountTv.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTv.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        } else {
            mProductPriceTv.setText(String.valueOf(product.getPrice() + ".-"));
        }
        mFavoriteBtn.setChecked(product.isFavorite());
        mPicasso.load(product.getImageUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(mProductImg, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        mPicasso.load(product.getImageUrl())
                                .into(mProductImg);
                    }
                });
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        mProductCountTv.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTv.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        if (isZoomed) {
            startZoomTransition();
            return true;
        }
        return false;
    }

    //endregion
}
