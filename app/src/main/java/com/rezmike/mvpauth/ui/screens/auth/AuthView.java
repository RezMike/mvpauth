package com.rezmike.mvpauth.ui.screens.auth;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;
import com.rezmike.mvpauth.mvp.views.IAuthView;
import com.rezmike.mvpauth.utils.ViewHelper;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import flow.Flow;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthView extends AbstractView<AuthScreen.AuthPresenter> implements IAuthView {

    public static final int IDLE_STATE = 0;
    public static final int LOGIN_STATE = 1;
    @Inject
    AuthScreen.AuthPresenter mPresenter;

    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.panel_wrapper)
    FrameLayout mPanelWrapper;
    @BindView(R.id.logo_img)
    ImageView mLogoImg;
    @BindView(R.id.app_name_txt)
    TextView mNameTv;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.vk_btn)
    ImageButton mVkBtn;
    @BindView(R.id.facebook_btn)
    ImageButton mFacebookBtn;
    @BindView(R.id.twitter_btn)
    ImageButton mTwitterBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;
    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailLayout;
    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordLayout;

    private AuthScreen mScreen;
    private Transition mBounds;
    private Transition mFade;
    private Subscription mAnimObs;
    private Animator mScaleAnimator;
    private float mDen;

    public AuthView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            mBounds = new ChangeBounds();
            mFade = new Fade();
            mScaleAnimator = AnimatorInflater.loadAnimator(getContext(), R.animator.logo_scale_animator);
            mDen = ViewHelper.getDensity(context);
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            setTextWatchers();
            setTypeFace();
            setAnimation();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.updateData();
            showViewFromState();
            startLogoAnim();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mAnimObs.unsubscribe();
        }
    }

    private void showViewFromState() {
        if (mScreen.isSplashTarget() && isIdle()) {
            showIdleState();
        } else {
            showLoginState();
        }
    }

    public void showLoginState() {
        CardView.LayoutParams cardParams = (LayoutParams) mAuthCard.getLayoutParams();
        cardParams.height = LayoutParams.MATCH_PARENT;
        mAuthCard.setLayoutParams(cardParams);
        mAuthCard.getChildAt(0).setVisibility(VISIBLE);
        mAuthCard.setCardElevation(4 * mDen);
        mShowCatalogBtn.setClickable(false);
        mShowCatalogBtn.setVisibility(GONE);
        mScreen.setCustomState(LOGIN_STATE);
    }

    public void showIdleState() {
        CardView.LayoutParams cardParams = (LayoutParams) mAuthCard.getLayoutParams();
        cardParams.height = (int) (44 * mDen);
        mAuthCard.setLayoutParams(cardParams);
        mAuthCard.getChildAt(0).setVisibility(GONE);
        mAuthCard.setCardElevation(0f);
        mShowCatalogBtn.setClickable(true);
        mShowCatalogBtn.setVisibility(VISIBLE);
        mScreen.setCustomState(IDLE_STATE);
        mEmailEt.setText("");
        mPasswordEt.setText("");
    }

    private void setTextWatchers() {
        mEmailEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mPresenter.checkEmailValid(s.toString());
            }
        });
        mPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mPresenter.checkPasswordValid(s.toString());
            }
        });
    }

    private void setTypeFace() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "bebas_neue_book.ttf");
        mNameTv.setTypeface(typeface);
    }

    private void setAnimation() {
        LayoutTransition layoutTransition = new LayoutTransition();
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        setLayoutTransition(layoutTransition);
    }

    //region ======================== Events ========================

    @OnClick(R.id.login_btn)
    void loginClick() {
        mPresenter.clickOnLogin();
    }

    @OnClick(R.id.vk_btn)
    void vkClick() {
        mPresenter.clickOnVk();
    }

    @OnClick(R.id.facebook_btn)
    void facebookClick() {
        mPresenter.clickOnFacebook();
    }

    @OnClick(R.id.twitter_btn)
    void twitterClick() {
        mPresenter.clickOnTwitter();
    }

    @OnClick(R.id.show_catalog_btn)
    void catalogClick() {
        mPresenter.clickOnShowCatalog();
    }

    //endregion

    //region ======================== Animation ========================

    public void showLoginWithAnim() {
        TransitionSet set = new TransitionSet();
        set.addTransition(mBounds)
                .addTransition(mFade)
                .setInterpolator(new FastOutSlowInInterpolator())
                .setOrdering(TransitionSet.ORDERING_SEQUENTIAL);
        TransitionManager.beginDelayedTransition(mPanelWrapper, set);
        showLoginState();
    }

    public void showIdleWithAnim() {
        TransitionSet set = new TransitionSet();
        Transition fade = new Fade();
        fade.addTarget(mAuthCard.getChildAt(0));
        set.addTransition(fade)
                .addTransition(mBounds)
                .addTransition(mFade)
                .setInterpolator(new FastOutSlowInInterpolator())
                .setOrdering(TransitionSet.ORDERING_SEQUENTIAL);
        TransitionManager.beginDelayedTransition(mPanelWrapper, set);
        showIdleState();
    }

    private void startLogoAnim() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AnimatedVectorDrawable avd = (AnimatedVectorDrawable) mLogoImg.getDrawable();
            mScaleAnimator.setTarget(mLogoImg);

            mAnimObs = Observable.interval(4000, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        mScaleAnimator.start();
                        avd.start();
                    });

            mScaleAnimator.start();
            avd.start();
        }
    }

    public void showInvalidLoginAnimation() {
        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.invalid_field_animator);
        set.setTarget(mAuthCard);
        set.start();
    }

    /**
     * Scene transition example
     * <p>
     * Transition tr = new Slide();
     * FrameLayout root = (FrameLayout) findViewById(R.id.panel_wrapper);
     * Scene authScene = Scene.getSceneForLayout(root, R.layout.auth_panel_scene, getContext());
     * TransitionManager.go(authScene, tr);
     */

    //endregion

    //region ======================== IAuthView ========================
    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(GONE);
    }

    @Override
    public String getUserEmail() {
        return mEmailEt.getText().toString();
    }

    @Override
    public String getUserPassword() {
        return mPasswordEt.getText().toString();
    }

    @Override
    public boolean isIdle() {
        return mScreen.getCustomState() == IDLE_STATE;
    }

    @Override
    public void setLoginEnabled(boolean enabled) {
        mLoginBtn.setEnabled(enabled);
        if (!enabled) showInvalidLoginAnimation();
    }

    @Override
    public void setEmailError(int errorStringId) {
        if (errorStringId == 0) {
            mEmailLayout.setError("");
            mEmailLayout.setErrorEnabled(false);
        } else {
            mEmailLayout.setError(getContext().getString(errorStringId));
        }
    }

    @Override
    public void setPasswordError(int errorStringId) {
        if (errorStringId == 0) {
            mPasswordLayout.setError("");
            mPasswordLayout.setErrorEnabled(false);
        } else {
            mPasswordLayout.setError(getContext().getString(errorStringId));
        }
    }

    @Override
    public void animateVkBtn() {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_anim);
        mVkBtn.startAnimation(animation);
    }

    @Override
    public void animateFacebookBtn() {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_anim);
        mFacebookBtn.startAnimation(animation);
    }

    @Override
    public void animateTwitterBtn() {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_anim);
        mTwitterBtn.startAnimation(animation);
    }

    public boolean isSplashTarget() {
        return mScreen.isSplashTarget();
    }

    @Override
    public boolean viewOnBackPressed() {
        if (mScreen.isSplashTarget() && !isIdle()) {
            showIdleWithAnim();
            return true;
        } else {
            return false;
        }
    }

    //endregion
}
