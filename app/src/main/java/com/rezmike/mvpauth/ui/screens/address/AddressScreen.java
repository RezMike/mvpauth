package com.rezmike.mvpauth.ui.screens.address;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.AddressDto;
import com.rezmike.mvpauth.data.storage.realm.AddressRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.scopes.DaggerScope;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.flow.Screen;
import com.rezmike.mvpauth.mvp.models.AccountModel;
import com.rezmike.mvpauth.mvp.presenters.AbstractPresenter;
import com.rezmike.mvpauth.mvp.presenters.IAddressPresenter;
import com.rezmike.mvpauth.ui.screens.account.AccountScreen;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;

@Screen(R.layout.screen_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {

    @Nullable
    private AddressRealm mAddressRealm;

    public AddressScreen(@Nullable AddressRealm addressRealm) {
        mAddressRealm = addressRealm;
    }

    @Override
    public boolean equals(Object o) {
        if (mAddressRealm != null) {
            return o instanceof AddressScreen && mAddressRealm.equals(((AddressScreen) o).mAddressRealm);
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return mAddressRealm != null ? mAddressRealm.hashCode() : super.hashCode();
    }

    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    //region ======================== DI ========================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(AddressScreen.class)
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter(mAddressRealm);
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @DaggerScope(AddressScreen.class)
    public interface Component {

        void inject(AddressPresenter presenter);

        void inject(AddressView view);
    }

    //endregion

    //region ======================== Presenter ========================

    public class AddressPresenter extends AbstractPresenter<AddressView, AccountModel> implements IAddressPresenter {

        private AddressRealm mAddress;

        AddressPresenter(AddressRealm addressRealm) {
            mAddress = addressRealm;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() == null) return;
            getView().showAddressView(mAddress != null ? new AddressDto(mAddress) : null);
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setActionBarTitle(getView().getContext().getString(R.string.address_title))
                    .setBackArrow(true)
                    .build();
        }

        @Override
        protected void initFab() {
            mRootPresenter.newFabBuilder()
                    .setVisable(false)
                    .build();
        }

        @Override
        public void clickOnAddAddress() {
            if (getView() == null) return;
            AddressRealm address = getView().getUserAddress();
            if (address != null) {
                mModel.updateOrInsertAddress(address);
                Flow.get(getView()).goBack();
            }
        }
    }

    //endregion

}
