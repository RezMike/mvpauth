package com.rezmike.mvpauth.ui.screens.catalog;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;
import com.rezmike.mvpauth.mvp.views.ICatalogView;
import com.rezmike.mvpauth.ui.screens.product.ProductView;

import butterknife.BindView;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class CatalogView extends AbstractView<CatalogScreen.CatalogPresenter> implements ICatalogView {

    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;

    private CatalogAdapter mAdapter;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mAdapter = new CatalogAdapter();
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mAdapter.registerDataSetObserver(mIndicator.getDataSetObserver());
    }

    @Nullable
    public CatalogAdapter getAdapter() {
        return mAdapter;
    }

    public int getCurrentPagerPosition() {
        return mProductPager.getCurrentItem();
    }

    //region ======================== Events ========================

    @OnClick(R.id.add_to_cart_btn)
    public void addToCartClick() {
        mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
    }

    //endregion

    //region ======================== ICatalogView ========================

    @Override
    public void showCatalogView() {
        mProductPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mProductPager);
    }

    @Override
    public void showCartIndicator() {
        if (mPresenter.getRootView() == null) return;
        mPresenter.getRootView().showBasketIndicator();
    }

    @Override
    public void updateProductCounter() {
        // TODO: 31.10.2016 update count product on cart icon
    }

    public ProductView getCurrentProductView() {
        return (ProductView) mProductPager.findViewWithTag(
                CatalogAdapter.PRODUCT_TAG_PREFIX + mProductPager.getCurrentItem());
    }

    @Override
    public boolean viewOnBackPressed() {
        return getCurrentProductView().viewOnBackPressed();
    }

    //endregion
}
