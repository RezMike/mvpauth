package com.rezmike.mvpauth.ui.screens.account;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.ActivityResultDto;
import com.rezmike.mvpauth.data.storage.dto.AddressDto;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.data.storage.dto.UserSettingsDto;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.scopes.DaggerScope;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.flow.Screen;
import com.rezmike.mvpauth.mvp.models.AccountModel;
import com.rezmike.mvpauth.mvp.presenters.AbstractPresenter;
import com.rezmike.mvpauth.mvp.presenters.IAccountPresenter;
import com.rezmike.mvpauth.mvp.presenters.RootPresenter;
import com.rezmike.mvpauth.ui.activities.RootActivity;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;
import com.rezmike.mvpauth.ui.screens.address.AddressScreen;
import com.rezmike.mvpauth.utils.ConstantManager;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = AccountView.PREVIEW_STATE;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    //region ======================== DI ========================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(AccountScreen.class)
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(AccountScreen.class)
    public interface Component {
        void inject(AccountPresenter presenter);

        void inject(AccountView view);

        void inject(AddressesAdapter adapter);

        AccountModel getAccountModel();

        RootPresenter getRootPresenter();
    }

    //endregion

    //region ======================== Presenter ========================

    public class AccountPresenter extends AbstractPresenter<AccountView, AccountModel> implements IAccountPresenter {

        private File mPhotoFile;
        private Subscription mActivityResultSub;

        //region ======================== Lifecycle ========================

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            subscribeOnActivityResult();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView();
            subscribeOnAddressesObs();
            subscribeOnSettingsObs();
            subscribeOnProfileInfoObs();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setActionBarTitle(getView().getContext().getString(R.string.account_title))
                    .addAction(new MenuItemHolder(
                            getView().getContext().getString(R.string.account_menu_edit),
                            R.drawable.ic_edit_black_24dp,
                            item -> {
                                switchViewState();
                                return true;
                            }
                    ))
                    .addAction(new MenuItemHolder(
                            getView().getContext().getString(R.string.actionbar_menu_basket),
                            R.drawable.ic_shopping_basket_black_24dp,
                            item -> {
                                if (getRootView() != null)
                                    getRootView().showMessage(R.string.actionbar_go_to_basket);
                                return true;
                            }
                    ))
                    .build();
        }

        @Override
        protected void initFab() {
            mRootPresenter.newFabBuilder()
                    .setVisable(false)
                    .build();
        }

        @Override
        protected void onExitScope() {
            mActivityResultSub.unsubscribe();
            super.onExitScope();
        }

        //endregion

        //region ======================== Subscriptions ========================

        private void subscribeOnAddressesObs() {
            Observable<AddressDto> addressDtoObs = mModel.getAddressObs()
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .map(AddressDto::new);
            mCompSubs.add(subscribe(addressDtoObs, new ViewSubscriber<AddressDto>() {
                @Override
                public void onNext(AddressDto address) {
                    if (getView() != null) {
                        getView().getAdapter().addItem(address);
                    }
                }
            }));
        }

        private void updateListView() {
            getView().getAdapter().reloadAdapter();
            subscribeOnAddressesObs();
        }

        private void subscribeOnSettingsObs() {
            mCompSubs.add(subscribe(mModel.getUserSettingsObs(), new ViewSubscriber<UserSettingsDto>() {
                @Override
                public void onNext(UserSettingsDto userSettingsDto) {
                    if (getView() != null) {
                        getView().initSettings(userSettingsDto);
                    }
                }
            }));
        }

        private void subscribeOnProfileInfoObs() {
            mCompSubs.add(subscribe(mModel.getProfileInfoSub(), new ViewSubscriber<ProfileInfoDto>() {
                @Override
                public void onNext(ProfileInfoDto profileInfoDto) {
                    if (getView() != null) {
                        getView().updateProfileInfo(profileInfoDto);
                    }
                }
            }));
        }

        private void subscribeOnActivityResult() {
            Observable<ActivityResultDto> activityResultObs = mRootPresenter.getActivityResultSubject()
                    .filter(activityResultDto -> activityResultDto.getResultCode() == Activity.RESULT_OK);

            mActivityResultSub = subscribe(activityResultObs, new ViewSubscriber<ActivityResultDto>() {
                @Override
                public void onNext(ActivityResultDto activityResultDto) {
                    handleActivityResult(activityResultDto);
                }
            });
        }

        private void handleActivityResult(ActivityResultDto activityResultDto) {
            if (getView() == null) return;
            switch (activityResultDto.getRequestCode()) {
                case ConstantManager.REQUEST_PROFILE_PHOTO_GALLERY:
                    if (activityResultDto.getIntent() != null) {
                        String photoUri = activityResultDto.getIntent().getData().toString();
                        getView().updateAvatarPhoto(Uri.parse(photoUri));
                    }
                    break;
                case ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA:
                    if (mPhotoFile != null) {
                        getView().updateAvatarPhoto(Uri.fromFile(mPhotoFile));
                    }
                    break;
            }
        }

        //endregion

        @Override
        public void clickOnAddAddress() {
            Flow.get(getView()).set(new AddressScreen(null));
        }

        @Override
        public void switchViewState() {
            if (getView() == null) return;
            if (getCustomState() == AccountView.EDIT_STATE) {
                mModel.saveProfileInfo(getView().getProfileInfo());
            }
            getView().changeState();
        }

        @Override
        public void switchSettings() {
            if (getView() == null) return;
            mModel.saveSettings(getView().getSettings());
        }

        @Override
        public void takePhoto() {
            if (getView() == null) return;
            getView().showPhotoSourceDialog();
        }

        //region ======================== GALLERY ========================

        @Override
        public void chooseGallery() {
            String[] permissions = new String[]{READ_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissionAndRequestIfNotGranted(
                    permissions, ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)) {
                mPhotoFile = createPhotoFile();
                if (mPhotoFile != null) {
                    takePhotoFromGallery();
                }
            }
        }

        private void takePhotoFromGallery() {
            if (getRootView() == null) return;
            Intent intent = new Intent();
            if (Build.VERSION.SDK_INT < 19) {
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
            } else {
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }
            ((RootActivity) getRootView()).startActivityForResult(
                    intent, ConstantManager.REQUEST_PROFILE_PHOTO_GALLERY);
        }

        //endregion

        //region ======================== CAMERA ========================

        @Override
        public void chooseCamera() {
            String[] permissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissionAndRequestIfNotGranted(
                    permissions, ConstantManager.REQUEST_PERMISSION_CAMERA)) {
                mPhotoFile = createPhotoFile();
                if (mPhotoFile != null) {
                    takePhotoFromCamera();
                }
            }
        }

        private void takePhotoFromCamera() {
            if (getRootView() == null) return;
            Uri uriForFile = FileProvider.getUriForFile((RootActivity) getRootView(),
                    ConstantManager.FILE_PROVIDER_AUTHORITY, mPhotoFile);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriForFile);
            ((RootActivity) getRootView()).startActivityForResult(
                    intent, ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA);
        }

        //endregion

        private File createPhotoFile() {
            if (getRootView() == null) return null;
            String timeStamp = SimpleDateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date());
            String imageFileName = "IMG_" + timeStamp;
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File fileImage;
            try {
                fileImage = File.createTempFile(imageFileName, ".jpg", storageDir);
            } catch (IOException e) {
                getRootView().showMessage(R.string.photo_error_create_file);
                return null;
            }
            return fileImage;
        }

        @Override
        public void editAddress(String addressId) {
            Flow.get(getView()).set(new AddressScreen(mModel.getAddress(addressId)));
        }

        @Override
        public void deleteAddress(String addressId) {
            mModel.deleteAddress(addressId);
            updateListView();
        }
    }

    //endregion
}
