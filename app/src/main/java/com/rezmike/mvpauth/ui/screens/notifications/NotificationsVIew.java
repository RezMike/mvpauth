package com.rezmike.mvpauth.ui.screens.notifications;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;

import butterknife.BindView;

public class NotificationsVIew extends AbstractView<NotificationsScreen.NotificationsPresenter> {

    @BindView(R.id.notifications_list)
    RecyclerView mNotificationsList;

    private final NotificationAdapter mAdapter = new NotificationAdapter((position, view) -> {
        if (view.getId() == R.id.revoke_btn) {
            mPresenter.clickOnRevoke(position);
        } else {
            mPresenter.clickOnStatus(position);
        }
    });

    public NotificationsVIew(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<NotificationsScreen.Component>getDaggerComponent(context).inject(this);
    }

    public void initView() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mNotificationsList.setLayoutManager(llm);
        mNotificationsList.setAdapter(mAdapter);
    }

    public NotificationAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
}
