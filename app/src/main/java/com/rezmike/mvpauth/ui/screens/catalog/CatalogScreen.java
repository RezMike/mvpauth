package com.rezmike.mvpauth.ui.screens.catalog;

import android.content.Context;
import android.os.Bundle;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.scopes.DaggerScope;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.flow.Screen;
import com.rezmike.mvpauth.mvp.models.CatalogModel;
import com.rezmike.mvpauth.mvp.presenters.AbstractPresenter;
import com.rezmike.mvpauth.mvp.presenters.ICatalogPresenter;
import com.rezmike.mvpauth.mvp.presenters.RootPresenter;
import com.rezmike.mvpauth.ui.activities.RootActivity;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;
import com.rezmike.mvpauth.ui.screens.auth.AuthScreen;
import com.rezmike.mvpauth.ui.screens.product.ProductScreen;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Subscription;

@Screen(R.layout.screen_catalog)
@DaggerScope(CatalogScreen.class)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region ======================== DI ========================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(CatalogScreen.class)
    public interface Component {
        void inject(CatalogPresenter presenter);

        void inject(CatalogView view);

        RootPresenter getRootPresenter();

        CatalogModel getCatalogModel();

        Picasso getPicasso();
    }

    //endregion

    //region ======================== Presenter ========================

    public class CatalogPresenter extends AbstractPresenter<CatalogView, CatalogModel> implements ICatalogPresenter {

        private int mLastPagerPosition;

        //region ======================== Lifecycle ========================

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().showCatalogView();
            mCompSubs.add(subscribeOnProductRealmObs());
        }

        @Override
        public void dropView(CatalogView view) {
            mLastPagerPosition = getView().getCurrentPagerPosition();
            super.dropView(view);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setActionBarTitle(getView().getContext().getString(R.string.catalog_actionbar_title))
                    .addAction(new MenuItemHolder(
                            getView().getContext().getString(R.string.actionbar_menu_basket),
                            R.drawable.ic_shopping_basket_black_24dp,
                            item -> {
                                if (getRootView() != null)
                                    getRootView().showMessage(R.string.actionbar_go_to_basket);
                                return true;
                            }
                    ))
                    .build();
        }

        @Override
        protected void initFab() {
            mRootPresenter.newFabBuilder()
                    .setVisable(false)
                    .build();
        }

        //endregion

        //region ======================== Subscriptions ========================

        private Subscription subscribeOnProductRealmObs() {
            if (getRootView() != null) {
                getRootView().showLoad();
            }
            return mModel.getProductObs()
                    .subscribe(new ViewSubscriber<ProductRealm>() {

                        CatalogAdapter mAdapter = getView().getAdapter();

                        @Override
                        public void onNext(ProductRealm productRealm) {
                            mAdapter.addItem(productRealm);
                            if (mAdapter.getCount() - 1 == mLastPagerPosition) {
                                getRootView().hideLoad();
                                getView().showCatalogView();
                            }
                        }
                    });
        }

        //endregion

        @Override
        public void clickOnBuyButton(int position) {
            if (getView() == null) return;
            if (checkUserAuth()) {
                getView().getCurrentProductView().startAddToCartAnimation();
                getView().showCartIndicator();
            } else {
                Flow.get(getView()).set(new AuthScreen(AuthScreen.LOGIN_TARGET));
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mModel.isUserAuth();
        }
    }

    //endregion

    public static class Factory {

        public static Context createProductContext(ProductRealm product, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            ProductScreen screen = new ProductScreen(product);
            String scopeName = String.format("%s_%s", screen.getScopeName(), product.getId());

            MortarScope childScope = parentScope.findChild(scopeName);
            if (childScope == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(DaggerService.<CatalogScreen.Component>getDaggerComponent(parentContext)))
                        .build(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}
