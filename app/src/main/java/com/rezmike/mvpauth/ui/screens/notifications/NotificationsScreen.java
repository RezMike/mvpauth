package com.rezmike.mvpauth.ui.screens.notifications;

import android.os.Bundle;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.scopes.DaggerScope;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.flow.Screen;
import com.rezmike.mvpauth.mvp.models.NotificationsModel;
import com.rezmike.mvpauth.mvp.presenters.AbstractPresenter;
import com.rezmike.mvpauth.ui.activities.RootActivity;

import dagger.Provides;
import mortar.MortarScope;

@Screen(R.layout.screen_notifications)
public class NotificationsScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerNotificationsScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region ======================== DI ========================

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(NotificationsScreen.class)
    public interface Component {
        void inject(NotificationsPresenter presenter);

        void inject(NotificationsVIew view);
    }

    @dagger.Module
    public class Module {

        @Provides
        @DaggerScope(NotificationsScreen.class)
        NotificationsPresenter provideNotificationsPresenter() {
            return new NotificationsPresenter();
        }

        @Provides
        @DaggerScope(NotificationsScreen.class)
        NotificationsModel provideNotificationsModel() {
            return new NotificationsModel();
        }
    }

    //endregion

    //region ======================== Presenter ========================

    public class NotificationsPresenter extends AbstractPresenter<NotificationsVIew, NotificationsModel> {

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setActionBarTitle(getView().getContext().getString(R.string.notifications_title))
                    .build();
        }

        @Override
        protected void initFab() {
            mRootPresenter.newFabBuilder()
                    .setVisable(false)
                    .build();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView();
            mCompSubs.add(mModel.getNotificationObs().subscribe(notificationRealm -> getView().getAdapter().addItem(notificationRealm)));
        }

        public void clickOnRevoke(int position) {
            // TODO: 04.05.2017 revoke order on server
            getRootView().showMessage("clickOnRevoke " + position);
        }

        public void clickOnStatus(int position) {
            // TODO: 04.05.2017 show order status
            getRootView().showMessage("clickOnRevoke " + position);
        }
    }

    //endregion

}
