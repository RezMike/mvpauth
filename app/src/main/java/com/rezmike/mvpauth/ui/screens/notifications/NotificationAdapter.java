package com.rezmike.mvpauth.ui.screens.notifications;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.realm.NotificationRealm;
import com.rezmike.mvpauth.utils.ConstantManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private final List<NotificationRealm> mNotifications = new ArrayList<>();
    private Context mContext;
    private final IListItemListener mItemListener;

    public NotificationAdapter(IListItemListener itemListener) {
        mItemListener = itemListener;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        mContext = recyclerView.getContext();
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void addItem(NotificationRealm notification) {
        mNotifications.add(notification);
        notifyDataSetChanged();
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new NotificationViewHolder(convertView, mItemListener);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        NotificationRealm notification = mNotifications.get(position);
        if (TextUtils.equals(notification.getType(), ConstantManager.NOTIFICATION_ORDER_TYPE)) {
            holder.notificationIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_av_timer_white_24dp));
            holder.revokeBtn.setVisibility(View.VISIBLE);
            holder.statusBtn.setText(mContext.getString(R.string.item_notification_status));
            holder.titleTv.setText(mContext.getString(R.string.item_notification_order_n) + notification.getMeta());
        } else {
            holder.notificationIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_whatshot_white_24dp));
            holder.revokeBtn.setVisibility(View.GONE);
            holder.statusBtn.setText(mContext.getString(R.string.item_notification_more));
            holder.titleTv.setText(notification.getTitle());
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yy");
        holder.dateTv.setText(sdf.format(notification.getDate()));
        holder.contentTv.setText(notification.getContent());
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.notification_icon)
        ImageView notificationIcon;
        @BindView(R.id.date_tv)
        TextView dateTv;
        @BindView(R.id.title_tv)
        TextView titleTv;
        @BindView(R.id.content_tv)
        TextView contentTv;
        @BindView(R.id.revoke_btn)
        Button revokeBtn;
        @BindView(R.id.status_btn)
        Button statusBtn;

        private final IListItemListener mListener;

        public NotificationViewHolder(View itemView, IListItemListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mListener = listener;
            revokeBtn.setOnClickListener(this);
            statusBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClicked(getAdapterPosition(), view);
            }
        }
    }

    public interface IListItemListener {
        void onItemClicked(int position, View view);
    }
}
