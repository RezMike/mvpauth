package com.rezmike.mvpauth.ui.screens.account;

import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.data.storage.dto.UserSettingsDto;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;
import com.rezmike.mvpauth.mvp.views.IAccountView;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import flow.Flow;

public class AccountView extends AbstractView<AccountScreen.AccountPresenter> implements IAccountView {

    public static final int EDIT_STATE = 0;
    public static final int PREVIEW_STATE = 1;

    @Inject
    Picasso mPicasso;

    @BindView(R.id.profile_name_txt)
    TextView mProfileNameTxt;
    @BindView(R.id.user_circle_avatar_img)
    CircleImageView mUserAvatarImg;
    @BindView(R.id.user_phone_et)
    EditText mUserPhoneEt;
    @BindView(R.id.user_full_name_et)
    EditText mUserFullNameEt;
    @BindView(R.id.profile_name_wrapper)
    LinearLayout mProfileNameWrapper;
    @BindView(R.id.address_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.notification_order_sw)
    SwitchCompat mNotificationOrderSw;
    @BindView(R.id.notification_promo_sw)
    SwitchCompat mNotificationPromoSw;

    private AccountScreen mScreen;
    private AddressesAdapter mAdapter;
    private Uri mAvatarUri;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
    }

    public AddressesAdapter getAdapter() {
        return mAdapter;
    }

    public void initView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new AddressesAdapter();
        mRecyclerView.setAdapter(mAdapter);
        initSwipe();
        showViewFromState();
    }

    private void initSwipe() {
        ItemSwipeCallback swipeCallback = new ItemSwipeCallback(getContext(), 0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                String addressId = mAdapter.getAddressId(position);

                if (direction == ItemTouchHelper.LEFT) {
                    showRemoveAddressDialog(addressId);
                } else {
                    showEditAddressDialog(addressId);
                }
            }
        };
        ItemTouchHelper touchHelper = new ItemTouchHelper(swipeCallback);
        touchHelper.attachToRecyclerView(mRecyclerView);
    }

    private void showEditAddressDialog(String addressId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder.setTitle(R.string.edit_address_dialog_title)
                .setMessage(R.string.edit_address_dialog_message)
                .setPositiveButton(R.string.edit_address_dialog_edit, (dialog, which) -> mPresenter.editAddress(addressId))
                .setNegativeButton(R.string.edit_address_dialog_cancel, (dialog, which) -> dialog.cancel())
                .setOnCancelListener(dialog -> mAdapter.notifyDataSetChanged())
                .show();
    }

    private void showRemoveAddressDialog(String addressId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder.setTitle(R.string.delete_address_dialog_title)
                .setMessage(R.string.delete_address_dialog_message)
                .setPositiveButton(R.string.delete_dialog_address_delete, (dialog, which) -> mPresenter.deleteAddress(addressId))
                .setNegativeButton(R.string.delete_address_dialog_cancel, (dialog, which) -> dialog.cancel())
                .setOnCancelListener(dialog -> mAdapter.notifyDataSetChanged())
                .show();
    }

    private void showViewFromState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            showPreviewState();
        } else {
            showEditState();
        }
    }

    //region ======================== Events ========================

    @OnClick(R.id.user_circle_avatar_img)
    void changeAvatarClick() {
        if (mScreen.getCustomState() == EDIT_STATE) {
            mPresenter.takePhoto();
        }
    }

    @OnClick(R.id.add_address_btn)
    void addAddressClick() {
        mPresenter.clickOnAddAddress();
    }

    //endregion

    //region ======================== IAccountView ========================

    @Override
    public void changeState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            mScreen.setCustomState(EDIT_STATE);
            showEditState();
        } else {
            mScreen.setCustomState(PREVIEW_STATE);
            showPreviewState();
        }
    }

    @Override
    public void showEditState() {
        mProfileNameWrapper.setVisibility(VISIBLE);

        mUserPhoneEt.setEnabled(true);
        mUserPhoneEt.setFocusable(true);
        mUserPhoneEt.setFocusableInTouchMode(true);

        mUserFullNameEt.setEnabled(true);
        mUserFullNameEt.setFocusable(true);
        mUserFullNameEt.setFocusableInTouchMode(true);

        mPicasso.load(R.drawable.ic_add_white_24dp)
                .placeholder(R.drawable.ic_add_white_24dp)
                .into(mUserAvatarImg);

        if (mPresenter.getRootView() != null) {
            mPresenter.getRootView().changeMenuItem(0, new MenuItemHolder(
                    getContext().getString(R.string.account_menu_save),
                    R.drawable.ic_done_black_24dp,
                    item -> {
                        mPresenter.switchViewState();
                        return true;
                    }
            ));
        }
    }

    @Override
    public void showPreviewState() {
        mProfileNameWrapper.setVisibility(GONE);

        mUserPhoneEt.setEnabled(false);
        mUserPhoneEt.setFocusable(false);
        mUserPhoneEt.setFocusableInTouchMode(false);

        mUserFullNameEt.setEnabled(false);
        mUserFullNameEt.setFocusable(false);
        mUserFullNameEt.setFocusableInTouchMode(false);

        insertAvatar();

        if (mPresenter.getRootView() != null) {
            mPresenter.getRootView().changeMenuItem(0, new MenuItemHolder(
                    getContext().getString(R.string.account_menu_edit),
                    R.drawable.ic_edit_black_24dp,
                    item -> {
                        mPresenter.switchViewState();
                        return true;
                    }
            ));
        }
    }

    @Override
    public void showPhotoSourceDialog() {
        String source[] = {
                getContext().getString(R.string.photo_dialog_upload_gallery),
                getContext().getString(R.string.photo_dialog_make_photo),
                getContext().getString(R.string.photo_dialog_cancel)
        };
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(R.string.photo_dialog_title);
        alertDialog.setItems(source, (dialog, which) -> {
            switch (which) {
                case 0:
                    mPresenter.chooseGallery();
                    break;
                case 1:
                    mPresenter.chooseCamera();
                    break;
                case 2:
                    dialog.cancel();
                    break;
            }
        });
        alertDialog.show();
    }

    @Override
    public ProfileInfoDto getProfileInfo() {
        return new ProfileInfoDto(
                mUserFullNameEt.getText().toString(),
                mUserPhoneEt.getText().toString(),
                String.valueOf(mAvatarUri)
        );
    }

    @Override
    public void updateProfileInfo(ProfileInfoDto profileInfo) {
        mProfileNameTxt.setText(profileInfo.getName());
        mUserFullNameEt.setText(profileInfo.getName());
        mUserPhoneEt.setText(profileInfo.getPhone());
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            mAvatarUri = Uri.parse(profileInfo.getAvatar());
            insertAvatar();
        }
    }

    @Override
    public UserSettingsDto getSettings() {
        return new UserSettingsDto(mNotificationOrderSw.isChecked(), mNotificationPromoSw.isChecked());
    }

    @Override
    public void initSettings(UserSettingsDto settings) {
        CompoundButton.OnCheckedChangeListener listener = (buttonView, isChecked) -> mPresenter.switchSettings();
        mNotificationOrderSw.setChecked(settings.isOrderNotification());
        mNotificationPromoSw.setChecked(settings.isPromoNotification());
        mNotificationOrderSw.setOnCheckedChangeListener(listener);
        mNotificationPromoSw.setOnCheckedChangeListener(listener);
    }

    @Override
    public void updateAvatarPhoto(Uri uri) {
        mAvatarUri = uri;
        insertAvatar();
    }

    private void insertAvatar() {
        mPicasso.load(mAvatarUri)
                .resize(140, 140)
                .centerCrop()
                .error(R.drawable.account_avatar)
                .placeholder(R.drawable.account_avatar)
                .into(mUserAvatarImg);
    }

    @Override
    public boolean viewOnBackPressed() {
        if (mScreen.getCustomState() == EDIT_STATE) {
            mPresenter.switchViewState();
            return true;
        } else {
            return false;
        }
    }

    //endregion
}
