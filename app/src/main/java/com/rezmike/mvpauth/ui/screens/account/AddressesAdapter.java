package com.rezmike.mvpauth.ui.screens.account;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.AddressDto;
import com.rezmike.mvpauth.di.DaggerService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressesAdapter extends RecyclerView.Adapter<AddressesAdapter.ViewHolder> {

    @Inject
    AccountScreen.AccountPresenter mAccountPresenter;

    private List<AddressDto> mUserAddresses;
    private Context mContext;

    public AddressesAdapter() {
        mUserAddresses = new ArrayList<>();
    }

    public void addItem(AddressDto address) {
        mUserAddresses.add(address);
        notifyDataSetChanged();
    }

    public void reloadAdapter() {
        mUserAddresses.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        mContext = recyclerView.getContext();
        DaggerService.<AccountScreen.Component>getDaggerComponent(mContext).inject(this);
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address, parent, false);
        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AddressDto address = mUserAddresses.get(position);
        holder.placeNameTv.setText(address.getName());
        holder.addressTv.setText(String.format(
                mContext.getString(R.string.item_address_format_string),
                address.getStreet(),
                address.getHouse(),
                address.getApartment(),
                address.getFloor()
        ));
        holder.commentTv.setText(address.getComment());
    }

    @Override
    public int getItemCount() {
        return mUserAddresses.size();
    }

    public String getAddressId(int position) {
        return mUserAddresses.get(position).getId();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.label_address_tv)
        TextView placeNameTv;
        @BindView(R.id.address_tv)
        TextView addressTv;
        @BindView(R.id.comment_tv)
        TextView commentTv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
