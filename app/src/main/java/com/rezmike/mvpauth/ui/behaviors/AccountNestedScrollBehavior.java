package com.rezmike.mvpauth.ui.behaviors;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.utils.ViewHelper;

@SuppressWarnings("unused")
public class AccountNestedScrollBehavior<V extends LinearLayout> extends AppBarLayout.ScrollingViewBehavior {
    private final int mMaxAppbarHeight;
    private final int mMinAppbarHeight;
    private final int mMaxPadding;

    public AccountNestedScrollBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        mMinAppbarHeight = ViewHelper.getStatusBarHeight(context) + ViewHelper.getActionBarHeight(context);
        mMaxAppbarHeight = context.getResources().getDimensionPixelSize(R.dimen.account_header_height);
        mMaxPadding = context.getResources().getDimensionPixelSize(R.dimen.account_info_padding);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        float friction = ViewHelper.currentFriction(mMinAppbarHeight, mMaxAppbarHeight, dependency.getBottom());
        int currentPadding = ViewHelper.lerp((float) mMaxPadding / 2, mMaxPadding, friction);

        child.setPadding(child.getPaddingLeft(), currentPadding, child.getPaddingRight(), child.getPaddingBottom());

        return super.onDependentViewChanged(parent, child, dependency);
    }
}
