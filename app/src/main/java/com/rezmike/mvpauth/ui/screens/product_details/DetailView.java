package com.rezmike.mvpauth.ui.screens.product_details;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;
import com.rezmike.mvpauth.mvp.views.IDetailView;

import butterknife.BindView;

public class DetailView extends AbstractView<DetailScreen.DetailPresenter> implements IDetailView {

    @BindView(R.id.detail_pager)
    ViewPager mViewPager;

    private ViewPager.OnPageChangeListener mListener;

    public DetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DetailScreen.Component>getDaggerComponent(context).inject(this);
    }

    public void initView(ProductRealm product) {
        DetailAdapter adapter = new DetailAdapter(product);
        mViewPager.setAdapter(adapter);
        mListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mPresenter.initFab();
            }
        };
        mViewPager.addOnPageChangeListener(mListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        mViewPager.removeOnPageChangeListener(mListener);
        mListener = null;
        super.onDetachedFromWindow();
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
}
