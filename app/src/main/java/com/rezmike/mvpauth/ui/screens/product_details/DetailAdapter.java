package com.rezmike.mvpauth.ui.screens.product_details;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rezmike.mvpauth.MvpApplication;
import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.ui.screens.product_details.comments.CommentsScreen;
import com.rezmike.mvpauth.ui.screens.product_details.description.DescriptionScreen;

import mortar.MortarScope;

public class DetailAdapter extends PagerAdapter {

    private ProductRealm mProduct;

    public DetailAdapter(ProductRealm product) {
        mProduct = product;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        AbstractScreen screen = null;

        switch (position) {
            case 0:
                screen = new DescriptionScreen(mProduct);
                break;
            case 1:
                screen = new CommentsScreen(mProduct);
                break;
        }

        MortarScope screenScope = createScreenScopeFromContext(container.getContext(), screen);
        Context screenContext = screenScope.createContext(container.getContext());
        View newView = LayoutInflater.from(screenContext).inflate(screen.getLayoutResId(), container, false);
        container.addView(newView);
        return newView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position) {
            case 0:
                title = MvpApplication.getContext().getString(R.string.description_title);
                break;
            case 1:
                title = MvpApplication.getContext().getString(R.string.comments_title);
                break;
        }
        return title;
    }

    private MortarScope createScreenScopeFromContext(Context context, AbstractScreen screen) {
        MortarScope parentScope = MortarScope.getScope(context);
        MortarScope childScope = parentScope.findChild(screen.getScopeName());

        if (childScope == null) {
            Object screenComponent = screen.createScreenComponent(parentScope.getService(DaggerService.SERVICE_NAME));
            if (screenComponent == null) {
                throw new IllegalStateException("Don't create component for " + screen.getScopeName());
            }

            childScope = parentScope.buildChild()
                    .withService(DaggerService.SERVICE_NAME, screenComponent)
                    .build(screen.getScopeName());
        }

        return childScope;
    }
}
