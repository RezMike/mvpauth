package com.rezmike.mvpauth.ui.screens.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.scopes.DaggerScope;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.flow.Screen;
import com.rezmike.mvpauth.mvp.models.AuthModel;
import com.rezmike.mvpauth.mvp.presenters.AbstractPresenter;
import com.rezmike.mvpauth.mvp.presenters.IAuthPresenter;
import com.rezmike.mvpauth.ui.activities.RootActivity;
import com.rezmike.mvpauth.ui.screens.catalog.CatalogScreen;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKServiceActivity;
import com.vk.sdk.api.VKError;

import java.util.Arrays;

import dagger.Provides;
import flow.Direction;
import flow.Flow;
import io.fabric.sdk.android.Fabric;
import mortar.MortarScope;
import rx.Subscription;

import static android.support.annotation.VisibleForTesting.NONE;

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {

    public static final int SPLASH_TARGET = 0;
    public static final int LOGIN_TARGET = 1;

    private int mCustomState = AuthView.IDLE_STATE;
    private int mTarget = SPLASH_TARGET;

    public AuthScreen(int target) {
        mTarget = target;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof AuthScreen && isSplashTarget() == ((AuthScreen) o).isSplashTarget() && super.equals(o);
    }

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    public boolean isSplashTarget() {
        return mTarget == SPLASH_TARGET;
    }

    //region ======================== DI ========================

    @dagger.Module
    public static class Module {

        @Provides
        @DaggerScope(AuthScreen.class)
        AuthPresenter providePresenter() {
            return new AuthPresenter();
        }

        @Provides
        @DaggerScope(AuthScreen.class)
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(AuthScreen.class)
    public interface Component {
        void inject(AuthPresenter presenter);

        void inject(AuthView view);
    }

    //endregion

    //region ======================== Presenter ========================

    public static class AuthPresenter extends AbstractPresenter<AuthView, AuthModel> implements IAuthPresenter {

        private static final String emailPattern = "^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%" +
                "&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7" +
                "f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" +
                "\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" +
                "\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x" +
                "0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])$";

        private static final String passwordPattern = "^\\S{8,}$";

        private LoginManager mLoginManager;
        private CallbackManager mCallbackManager;
        private TwitterAuthClient mTwitterAuthClient;
        private boolean isEmailValid = true;
        private boolean isPasswordValid = true;
        private boolean isTestMode;

        public AuthPresenter() {
        }

        @VisibleForTesting(otherwise = NONE)
        public AuthPresenter(LoginManager loginManager, CallbackManager callbackManager) {
            mLoginManager = loginManager;
            mCallbackManager = callbackManager;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
            if (!isTestMode) initSocialSdk();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        public void updateData() {
            if (getView().isSplashTarget()) mModel.startUpdateLocalData();
        }

        @Override
        protected void initActionBar() {
            if (getView().isSplashTarget()) {
                mRootPresenter.newActionBarBuilder()
                        .setVisable(false)
                        .build();
            } else {
                mRootPresenter.newActionBarBuilder()
                        .setVisable(true)
                        .setActionBarTitle(getView().getContext().getString(R.string.auth_title))
                        .setBackArrow(true)
                        .build();
            }
        }

        @Override
        protected void initFab() {
            mRootPresenter.newFabBuilder()
                    .setVisable(false)
                    .build();
        }

        @Override
        public void clickOnLogin() {
            if (getView() == null) return;
            if (getView().isIdle()) {
                getView().showLoginWithAnim();
            } else {
                if (isEmailAndPasswordValid()) {
                    // TODO: 23.10.2016 auth user
                    mModel.loginUser(getView().getUserEmail(), getView().getUserPassword())
                            .subscribe(userRes -> {
                                    }, throwable -> {
                                        if (getRootView() != null) getRootView().showError(throwable);
                                    },
                                    this::onLoginSuccess);
                } else {
                    getView().showInvalidLoginAnimation();
                }
            }
        }

        @Override
        public void clickOnVk() {
            if (getView() == null || getRootView() == null) return;
            if (!mModel.isUserAuth()) {
                VKSdk.login((Activity) getRootView(), "email");
            } else {
                getRootView().showMessage("Вы уже авторизованы как " + mModel.getUserName());
            }
            getView().animateVkBtn();
        }

        @Override
        public void clickOnFacebook() {
            if (getView() == null || getRootView() == null) return;
            if (!mModel.isUserAuth()) {
                mLoginManager.logInWithReadPermissions((Activity) getRootView(), Arrays.asList("email"));
            } else {
                getRootView().showMessage("Вы уже авторизованы как " + mModel.getUserName());
            }
            getView().animateFacebookBtn();
        }

        @Override
        public void clickOnTwitter() {
            if (getView() == null || getRootView() == null) return;
            if (!mModel.isUserAuth()) {
                mTwitterAuthClient.authorize((Activity) getRootView(), twitterCallback);
            } else {
                getRootView().showMessage("Вы уже авторизованы как " + mModel.getUserName());
            }
            getView().animateFacebookBtn();
            getView().animateTwitterBtn();
        }

        @Override
        public void clickOnShowCatalog() {
            if (getView() == null || getRootView() == null) return;
            if (mModel.isUserAuth()) {
                Flow.get(getView().getContext()).replaceHistory(new CatalogScreen(), Direction.REPLACE);
            } else {
                Flow.get(getView().getContext()).set(new CatalogScreen());
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mModel.isUserAuth();
        }

        public void onLoginSuccess() {
            if (getView() == null) return;
            getView().hideLoginBtn();
            getView().showIdleWithAnim();
        }

        @Override
        public void checkEmailValid(String email) {
            if (getView() == null) return;
            if (!email.isEmpty() && !email.matches(emailPattern)) {
                getView().setEmailError(R.string.auth_email_not_valid);
                isEmailValid = false;
            } else {
                getView().setEmailError(0);
                isEmailValid = true;
            }
            checkLogin();
        }

        @Override
        public void checkPasswordValid(String password) {
            if (getView() == null) return;
            if (password.isEmpty())
                if (!password.isEmpty() && !password.matches(passwordPattern)) {
                    getView().setPasswordError(R.string.auth_password_not_valid);
                    isPasswordValid = false;
                } else {
                    getView().setPasswordError(0);
                    isPasswordValid = true;
                }
            checkLogin();
        }

        private void checkLogin() {
            getView().setLoginEnabled(isEmailValid && isPasswordValid);
        }

        private boolean isEmailAndPasswordValid() {
            if (getView().getUserEmail().length() == 0) {
                getView().setEmailError(R.string.auth_email_empty);
                isEmailValid = false;
            }
            if (getView().getUserPassword().length() == 0) {
                getView().setPasswordError(R.string.auth_password_empty);
                isPasswordValid = false;
            }
            return isEmailValid && isPasswordValid;
        }

        //region ======================== Social ========================

        @Override
        public void initSocialSdk() {
            if (mLoginManager == null) mLoginManager = LoginManager.getInstance();
            if (mCallbackManager == null) mCallbackManager = CallbackManager.Factory.create();
            if (mTwitterAuthClient == null) {
                Fabric.with(getView().getContext(), new Twitter(new TwitterAuthConfig("GokBvTUa66MomjU9PBPZETg7L", "vfpTYzxfRO2FfHwoThNPOzXOcwc59QuWSOC3U4NRJ2ixt6tUqi")));
                mTwitterAuthClient = new TwitterAuthClient();
            }
            mLoginManager.registerCallback(mCallbackManager, facebookCallback);
            mCompSubs.add(subscribeOnActivityResult());
        }

        private Subscription subscribeOnActivityResult() {
            return mRootPresenter.getActivityResultSubject()
                    .subscribe(activityResultDto -> {
                        int requestCode = activityResultDto.getRequestCode();
                        int resultCode = activityResultDto.getResultCode();
                        Intent intent = activityResultDto.getIntent();

                        if (requestCode == VKServiceActivity.VKServiceType.Authorization.getOuterCode()) {
                            VKSdk.onActivityResult(requestCode, resultCode, intent, vkCallback);
                        }

                        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
                            mCallbackManager.onActivityResult(requestCode, resultCode, intent);
                            Log.e("Facebook", "onActivityResult");
                        }

                        if (resultCode == Activity.RESULT_CANCELED &&
                                (requestCode == VKServiceActivity.VKServiceType.Authorization.getOuterCode() || requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode())) {
                            onSocialCancel();
                        }
                    }, throwable -> {
                        if (getRootView() != null)
                            getRootView().showError(throwable);
                    });
        }

        private VKCallback<VKAccessToken> vkCallback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                onSocialResult(res, SocialSdkType.VK);
            }

            @Override
            public void onError(VKError error) {
                onSocialError(error, SocialSdkType.VK);
            }
        };

        private FacebookCallback<LoginResult> facebookCallback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                onSocialResult(loginResult, SocialSdkType.FACEBOOK);
            }

            @Override
            public void onCancel() {
                onSocialCancel();
            }

            @Override
            public void onError(FacebookException error) {
                onSocialError(error, SocialSdkType.FACEBOOK);
            }
        };

        private Callback<TwitterSession> twitterCallback = new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                onSocialResult(result.data, SocialSdkType.TWITTER);
            }

            @Override
            public void failure(TwitterException exception) {
                onSocialError(exception, SocialSdkType.TWITTER);
            }
        };

        @VisibleForTesting(otherwise = NONE)
        public void testSocial() {
            isTestMode = true;
        }

        @Override
        public void onSocialResult(Object res, SocialSdkType type) {
            switch (type) {
                case VK:
                    VKAccessToken vkRes = (VKAccessToken) res;
                    mModel.loginVk(vkRes.accessToken, vkRes.userId, vkRes.email)
                            .subscribe(userRes -> {
                                if (getRootView() != null)
                                    getRootView().showMessage("Вы успешно авторизованы как " + userRes.getFullName());
                            }, throwable -> {
                                if (getRootView() != null)
                                    getRootView().showError(throwable);
                            }, () -> {
                                mRootPresenter.updateUserInfo();
                            });
                    break;
                case FACEBOOK:
                    LoginResult facebookRes = (LoginResult) res;
                    mModel.loginFacebook(facebookRes.getAccessToken().getToken(), facebookRes.getAccessToken().getUserId())
                            .subscribe(userRes -> {
                                if (getRootView() != null)
                                    getRootView().showMessage("Вы успешно авторизованы как " + userRes.getFullName());
                            }, throwable -> {
                                if (getRootView() != null)
                                    getRootView().showError(throwable);
                            }, () -> {
                                mRootPresenter.updateUserInfo();
                            });
                    ;
                    break;
                case TWITTER:
                    TwitterSession session = (TwitterSession) res;
                    mModel.loginTwitter(session)
                            .subscribe(userRes -> {
                                if (getRootView() != null)
                                    getRootView().showMessage("Вы успешно авторизованы как " + userRes.getFullName());
                            }, throwable -> {
                                if (getRootView() != null)
                                    getRootView().showError(throwable);
                            }, () -> {
                                mRootPresenter.updateUserInfo();
                            });
                    ;
                    break;
            }
        }

        @Override
        public void onSocialError(Object res, SocialSdkType type) {
            if (getRootView() == null) return;
            getRootView().showMessage("Ошибка авторизации");
        }

        @Override
        public void onSocialCancel() {
            if (getRootView() == null) return;
            getRootView().showMessage("Авторизация отменена");
        }

        //endregion
    }

    //endregion
}
