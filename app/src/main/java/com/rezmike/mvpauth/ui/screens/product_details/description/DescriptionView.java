package com.rezmike.mvpauth.ui.screens.product_details.description;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.AttributeSet;
import android.widget.TextView;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.DescriptionDto;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.mvp.views.AbstractView;

import butterknife.BindView;
import butterknife.OnClick;

public class DescriptionView extends AbstractView<DescriptionScreen.DescriptionPresenter> {

    @BindView(R.id.description_tv)
    TextView mDescriptionTv;
    @BindView(R.id.product_count_tv)
    TextView mProductCountTv;
    @BindView(R.id.product_price_tv)
    TextView mProductPriceTv;
    @BindView(R.id.product_rating)
    AppCompatRatingBar mProductRating;

    public DescriptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DescriptionScreen.Component>getDaggerComponent(context).inject(this);
    }

    public void initView(DescriptionDto descriptionDto) {
        mDescriptionTv.setText(descriptionDto.getDescription());
        mProductRating.setRating(descriptionDto.getRating());
        mProductCountTv.setText(String.valueOf(descriptionDto.getCount()));

        if (descriptionDto.getCount() > 0) {
            mProductPriceTv.setText(String.valueOf(descriptionDto.getCount() * descriptionDto.getPrice() + ".-"));
        } else {
            mProductPriceTv.setText(String.valueOf(descriptionDto.getPrice() + ".-"));
        }
    }

    //region ======================== Events ========================

    @OnClick(R.id.plus_btn)
    void clickOnPlus() {
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.minus_btn)
    void clickOnMinus() {
        mPresenter.clickOnMinus();
    }

    //endregion

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
}
