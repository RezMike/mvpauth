package com.rezmike.mvpauth.ui.screens.product;

import android.os.Bundle;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.ProductDto;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.scopes.DaggerScope;
import com.rezmike.mvpauth.flow.AbstractScreen;
import com.rezmike.mvpauth.flow.Screen;
import com.rezmike.mvpauth.mvp.models.CatalogModel;
import com.rezmike.mvpauth.mvp.presenters.AbstractPresenter;
import com.rezmike.mvpauth.mvp.presenters.IProductPresenter;
import com.rezmike.mvpauth.ui.screens.catalog.CatalogScreen;
import com.rezmike.mvpauth.ui.screens.product_details.DetailScreen;

import dagger.Provides;
import flow.Flow;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import mortar.MortarScope;

@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.Component> {

    private ProductRealm mProductRealm;

    public ProductScreen(ProductRealm product) {
        mProductRealm = product;
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProductRealm.equals(((ProductScreen) o).getProductRealm());
    }

    @Override
    public int hashCode() {
        return mProductRealm.hashCode();
    }

    public ProductRealm getProductRealm() {
        return mProductRealm;
    }

    //region ======================== DI ========================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(ProductScreen.class)
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductRealm);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @DaggerScope(ProductScreen.class)
    public interface Component {
        void inject(ProductPresenter presenter);

        void inject(ProductView view);
    }

    //endregion

    //region ======================== Presenter ========================

    public class ProductPresenter extends AbstractPresenter<ProductView, CatalogModel> implements IProductPresenter {

        private RealmChangeListener mListener;
        private ProductRealm mProduct;

        public ProductPresenter(ProductRealm productRealm) {
            mProduct = productRealm;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null && mProduct.isValid()) {
                getView().showProductView(new ProductDto(mProduct));

                mListener = element -> {
                    if (getView() == null) return;
                    getView().showProductView(new ProductDto(mProduct));
                };
                mProduct.addChangeListener(mListener);
            } else {
                // TODO: 18.01.2017 implement this
            }
        }

        @Override
        public void dropView(ProductView view) {
            mProduct.removeChangeListener(mListener);
            super.dropView(view);
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            //empty
        }

        @Override
        protected void initFab() {
            //empty
        }

        @Override
        public void clickOnPlus() {
            if (getView() == null) return;
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> mProduct.add());
            realm.close();
        }

        @Override
        public void clickOnMinus() {
            if (getView() == null) return;
            if (mProduct.getCount() > 0) {
                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(realm1 -> mProduct.remove());
                realm.close();
            }
        }

        @Override
        public void clickOnFavorite() {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> mProduct.changeFavorite());
            realm.close();
        }

        @Override
        public void clickOnShowMore() {
            Flow.get(getView()).set(new DetailScreen(mProduct));
        }
    }

    //endregion
}
