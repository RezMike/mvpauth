package com.rezmike.mvpauth.ui.screens.product_details.comments;

import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.storage.dto.CommentDto;
import com.rezmike.mvpauth.di.DaggerService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {

    @Inject
    Picasso mPicasso;

    private List<CommentDto> mCommentsList = new ArrayList<>();

    public void addItem(CommentDto commentDto) {
        mCommentsList.add(commentDto);
        Collections.sort(mCommentsList, (comment1, comment2) ->
                (int) (comment2.getCommentDate().getTime() - comment1.getCommentDate().getTime()));
        notifyDataSetChanged();
    }

    public void reloadAdapter(List<CommentDto> comments) {
        mCommentsList.clear();
        mCommentsList = comments;
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        DaggerService.<CommentsScreen.Component>getDaggerComponent(recyclerView.getContext()).inject(this);
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        CommentDto comment = mCommentsList.get(position);
        holder.userNameTv.setText(comment.getUserName());
        holder.dateTv.setText(DateUtils.getRelativeTimeSpanString(comment.getCommentDate().getTime()));
        holder.commentTv.setText(comment.getComment());
        holder.rating.setRating(comment.getRating());

        String avatarUrl = comment.getAvatarUrl();
        if (avatarUrl == null || avatarUrl.isEmpty()) {
            avatarUrl = "null";
        }
        mPicasso.load(avatarUrl)
                .fit()
                .placeholder(R.drawable.ic_account_circle_black_24dp)
                .error(R.drawable.ic_account_circle_black_24dp)
                .into(holder.avatarImg);
    }

    @Override
    public int getItemCount() {
        return mCommentsList.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.comment_avatar_img)
        ImageView avatarImg;
        @BindView(R.id.user_name_tv)
        TextView userNameTv;
        @BindView(R.id.date_tv)
        TextView dateTv;
        @BindView(R.id.comment_tv)
        TextView commentTv;
        @BindView(R.id.rating)
        AppCompatRatingBar rating;

        public CommentViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
