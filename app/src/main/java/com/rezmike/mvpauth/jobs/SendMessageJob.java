package com.rezmike.mvpauth.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.data.network.res.CommentRes;
import com.rezmike.mvpauth.data.storage.realm.CommentRealm;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;
import com.rezmike.mvpauth.utils.AppConfig;

import java.util.Date;

import io.realm.Realm;

public class SendMessageJob extends Job {

    private static final String TAG = "SendMessageJob";

    private final String mProductId;
    private final CommentRealm mComment;

    public SendMessageJob(String productId, CommentRealm comment) {
        super(new Params(JobPriority.MID)
                .requireNetwork()
                .persist()
                .groupBy("Comments"));
        mComment = comment;
        mProductId = productId;
    }

    @Override
    public void onAdded() {
        Log.e(TAG, "SEND MESSAGE onAdded: ");
        Realm realm = Realm.getDefaultInstance();
        ProductRealm product = realm.where(ProductRealm.class)
                .equalTo("id", mProductId)
                .findFirst();

        realm.executeTransaction(realm1 -> product.getComments().add(mComment));
        realm.close();
    }

    @Override
    public void onRun() throws Throwable {
        Log.e(TAG, "SEND MESSAGE onRun: ");

        CommentRes comment = new CommentRes(mComment);
        DataManager.getInstance().sendComment(mProductId, comment)
                .subscribe(commentRes -> {
                    Realm realm = Realm.getDefaultInstance();
                    CommentRealm localComment = realm.where(CommentRealm.class)
                            .equalTo("id", mComment.getId())
                            .findFirst();
                    ProductRealm product = realm.where(ProductRealm.class)
                            .equalTo("id", mProductId)
                            .findFirst();
                    CommentRealm serverComment = new CommentRealm(commentRes);
                    realm.executeTransaction(realm1 -> {
                        if (localComment.isValid()) localComment.deleteFromRealm();
                        if (product.isValid()) product.getComments().add(serverComment);
                    });
                    realm.close();
                });
    }

    @Override
    protected void onCancel(int i, @Nullable Throwable throwable) {
        Log.e(TAG, "SEND MESSAGE onCancel: ");
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        Log.e(TAG, "SEND MESSAGE shouldReRunOnThrowable: " + runCount + " " + maxRunCount + " s " + new Date());
        return RetryConstraint.createExponentialBackoff(runCount, AppConfig.JOB_INITIAL_BACK_OFF_IN_MS);
    }
}
