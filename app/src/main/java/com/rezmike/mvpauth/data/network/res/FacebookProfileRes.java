package com.rezmike.mvpauth.data.network.res;

import com.squareup.moshi.Json;

public class FacebookProfileRes {
    private String email;
    private FacebookPicture picture;
    @Json(name = "first_name")
    private String firstName;
    @Json(name = "last_name")
    private String lastName;
    private String id;

    private static class FacebookPicture {
        FacebookPictureData data;

        private static class FacebookPictureData {
            int height;
            @Json(name = "is_silhouette")
            boolean isSilhouette;
            String url;
            int width;
        }
    }

    public String getEmail() {
        return email;
    }

    public String getAvatar() {
        return picture.data.url;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
