package com.rezmike.mvpauth.data.storage.realm;

import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.data.network.res.CommentRes;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CommentRealm extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String userName;
    private String avatar;
    private float rating;
    private Date commentDate;
    private String comment;

    public CommentRealm() {
    }

    public CommentRealm(float rating, String comment) {
        ProfileInfoDto profileInfo = DataManager.getInstance().getProfileInfo();
        this.id = String.valueOf(this.hashCode());
        this.userName = profileInfo.getName();
        this.avatar = profileInfo.getAvatar();
        this.rating = rating;
        this.commentDate = new Date();
        this.comment = comment;
    }

    public CommentRealm(CommentRes commentRes) {
        this.id = commentRes.getId();
        this.userName = commentRes.getUserName();
        this.avatar = commentRes.getAvatar();
        this.rating = commentRes.getRating();
        this.commentDate = commentRes.getCommentDate();
        this.comment = commentRes.getComment();
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public float getRating() {
        return rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }
}
