package com.rezmike.mvpauth.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.rezmike.mvpauth.data.network.res.UserRes;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.data.storage.dto.UserSettingsDto;

public class PreferencesManager {

    static final String PROFILE_AUTH_TOKEN_KEY = "PROFILE_AUTH_TOKEN_KEY";
    static final String PROFILE_USER_ID_KEY = "PROFILE_USER_ID_KEY";
    static final String PROFILE_NAME_KEY = "PROFILE_NAME_KEY";
    static final String PROFILE_AVATAR_KEY = "PROFILE_AVATAR_KEY";
    static final String PROFILE_PHONE_KEY = "PROFILE_PHONE_KEY";
    static final String ORDER_NOTIFICATION_KEY = "ORDER_NOTIFICATION_KEY";
    static final String PROMO_NOTIFICATION_KEY = "PROMO_NOTIFICATION_KEY";
    static final String PRODUCT_LAST_UPDATE_KEY = "PRODUCT_LAST_UPDATE_KEY";

    static final String DEFAULT_LAST_UPDATE = "Thu Jan 1 1970 00:00:00 GMT+0000 (UTC)";

    private final SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isUserAuth() {
        return getAuthToken() != null;
    }

    public String getAuthToken() {
        return mSharedPreferences.getString(PROFILE_AUTH_TOKEN_KEY, null);
    }

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PROFILE_AUTH_TOKEN_KEY, authToken);
        editor.apply();
    }

    public void deleteAuthToken() {
        saveAuthToken(null);
    }

    public String getLastProductUpdate() {
        return mSharedPreferences.getString(PRODUCT_LAST_UPDATE_KEY, DEFAULT_LAST_UPDATE);
    }

    public void saveLastProductUpdate(String lastModified) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PRODUCT_LAST_UPDATE_KEY, lastModified);
        editor.apply();
    }

    public ProfileInfoDto getProfileInfo() {
        return new ProfileInfoDto(
                mSharedPreferences.getString(PROFILE_NAME_KEY, "Unregistered user"),
                mSharedPreferences.getString(PROFILE_PHONE_KEY, ""),
                mSharedPreferences.getString(PROFILE_AVATAR_KEY, "null")
        );
    }

    public void saveProfileInfo(ProfileInfoDto profileInfo) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PROFILE_NAME_KEY, profileInfo.getName());
        editor.putString(PROFILE_PHONE_KEY, profileInfo.getPhone());
        editor.putString(PROFILE_AVATAR_KEY, profileInfo.getAvatar());
        editor.apply();
    }

    public void saveProfileInfo(UserRes user) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PROFILE_USER_ID_KEY, user.getId());
        editor.putString(PROFILE_NAME_KEY, user.getFullName());
        editor.putString(PROFILE_PHONE_KEY, user.getPhone());
        editor.putString(PROFILE_AVATAR_KEY, user.getAvatarUrl());
        editor.putString(PROFILE_AUTH_TOKEN_KEY, user.getToken());
        editor.apply();
    }

    public String getUserName() {
        return mSharedPreferences.getString(PROFILE_NAME_KEY, "Unregistered user");
    }

    public String getUserAvatar() {
        return mSharedPreferences.getString(PROFILE_AVATAR_KEY, "null");
    }

    public void saveUserAvatar(String avatarUrl) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PROFILE_AVATAR_KEY, avatarUrl);
        editor.apply();
    }

    public UserSettingsDto getUserSettings() {
        return new UserSettingsDto(
                mSharedPreferences.getBoolean(ORDER_NOTIFICATION_KEY, false),
                mSharedPreferences.getBoolean(PROMO_NOTIFICATION_KEY, false)
        );
    }

    public void saveSettings(UserSettingsDto settings) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(ORDER_NOTIFICATION_KEY, settings.isOrderNotification());
        editor.putBoolean(PROMO_NOTIFICATION_KEY, settings.isPromoNotification());
        editor.apply();
    }
}
