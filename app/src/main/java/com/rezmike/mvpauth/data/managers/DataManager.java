package com.rezmike.mvpauth.data.managers;

import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.rezmike.mvpauth.MvpApplication;
import com.rezmike.mvpauth.data.network.RestCallTransformer;
import com.rezmike.mvpauth.data.network.RestService;
import com.rezmike.mvpauth.data.network.req.SocialLoginReq;
import com.rezmike.mvpauth.data.network.req.UserLoginReq;
import com.rezmike.mvpauth.data.network.res.AvatarUrlRes;
import com.rezmike.mvpauth.data.network.res.CommentRes;
import com.rezmike.mvpauth.data.network.res.FacebookProfileRes;
import com.rezmike.mvpauth.data.network.res.ProductRes;
import com.rezmike.mvpauth.data.network.res.TwitterProfileRes;
import com.rezmike.mvpauth.data.network.res.UserRes;
import com.rezmike.mvpauth.data.network.res.VkProfileRes;
import com.rezmike.mvpauth.data.storage.dto.FacebookDataDto;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.data.storage.dto.UserSettingsDto;
import com.rezmike.mvpauth.data.storage.dto.VkDataDto;
import com.rezmike.mvpauth.data.storage.realm.AddressRealm;
import com.rezmike.mvpauth.data.storage.realm.NotificationRealm;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.components.DaggerDataManagerComponent;
import com.rezmike.mvpauth.di.components.DataManagerComponent;
import com.rezmike.mvpauth.di.modules.LocalModule;
import com.rezmike.mvpauth.di.modules.NetworkModule;
import com.rezmike.mvpauth.utils.AppConfig;
import com.rezmike.mvpauth.utils.NetworkStatusChecker;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;
import rx.schedulers.Schedulers;

import static android.support.annotation.VisibleForTesting.NONE;

public class DataManager {

    private static final String TAG = "DataManager";

    private static DataManager ourInstance;
    private RestCallTransformer mRestCallTransformer;

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RealmManager mRealmManager;
    @Inject
    RestService mRestService;
    @Inject
    Retrofit mRetrofit;

    public static DataManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new DataManager();
        }
        return ourInstance;
    }

    @VisibleForTesting(otherwise = NONE)
    DataManager(RestService restService) {
        mRestService = restService;
        mRestCallTransformer = new RestCallTransformer<>();
        ourInstance = this;
    }

    @VisibleForTesting(otherwise = NONE)
    DataManager(RestService restService, PreferencesManager preferencesManager, RealmManager realmManager) {
        mRestService = restService;
        mPreferencesManager = preferencesManager;
        mRealmManager = realmManager;
        mRestCallTransformer = new RestCallTransformer<>();
        mRestCallTransformer.setTestMode();
        ourInstance = this;
    }

    private DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if (component == null) {
            component = DaggerDataManagerComponent.builder()
                    .appComponent(MvpApplication.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);
        mRestCallTransformer = new RestCallTransformer<>();
    }

    public void startUpdateLocalDataWithTimer() {
        Log.e(TAG, "LOCAL UPDATE start: " + new Date());
        Observable.interval(AppConfig.JOB_UPDATE_DATA_INTERVAL, TimeUnit.SECONDS)
                .flatMap(aLong -> NetworkStatusChecker.isInternetAvailable())
                .filter(aBoolean -> aBoolean)
                .flatMap(aBoolean -> getProductsObsFromNetwork())
                .subscribe(productRealm -> {
                    Log.e(TAG, "LOCAL UPDATE complete: ");
                }, throwable -> {
                    throwable.printStackTrace();
                    Log.e(TAG, "LOCAL UPDATE error: " + throwable.getMessage());
                });
    }

    //region ======================== Getters ========================

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    //endregion

    //region ======================== Login ========================

    public boolean isUserAuth() {
        return false;
        //return !mPreferencesManager.isUserAuth();
    }

    public Observable<UserRes> loginUser(UserLoginReq userLoginReq) {
        return mRestService.loginUser(userLoginReq)
                .compose((RestCallTransformer<UserRes>) mRestCallTransformer)
                .doOnNext(userRes -> mPreferencesManager.saveProfileInfo(userRes));
    }

    public Observable<UserRes> socialLogin(SocialLoginReq loginReq) {
        return mRestService.socialLogin(loginReq)
                .compose((RestCallTransformer<UserRes>) mRestCallTransformer)
                .retryWhen(errorObservable -> errorObservable
                        .zipWith(Observable.range(1, AppConfig.RETRY_REQUEST_COUNT), (throwable, retryCount) -> retryCount)
                        .doOnNext(retryCount -> Log.e(TAG, "LOCAL UPDATE request retry count: " + retryCount + " " + new Date()))
                        .map(retryCount -> (long) (AppConfig.RETRY_REQUEST_BASE_DELAY * Math.pow(Math.E, retryCount)))
                        .doOnNext(delay -> Log.e(TAG, "LOCAL UPDATE delay: " + delay))
                        .flatMap(delay -> Observable.timer(delay, TimeUnit.MILLISECONDS))
                );
    }

    public Observable<VkProfileRes> getVkProfile(String baseUrl, VkDataDto vkDataDto) {
        return mRestService.getVkProfile(baseUrl, vkDataDto.getAccessToken());
    }

    public Observable<FacebookProfileRes> getFacebookProfile(String baseUrl, FacebookDataDto facebookDataDto) {
        return mRestService.getFacebookProfile(baseUrl, facebookDataDto.getAccessToken());
    }

    private Observable<TwitterProfileRes> getTwitterProfile(TwitterSession session) {
        return Observable.create(singleSubscriber -> {
            try {
                Response<User> response = Twitter.getApiClient(session).getAccountService().verifyCredentials(true, false).execute();
                User user = response.body();
                TwitterProfileRes res = new TwitterProfileRes(user.name, user.email, user.profileImageUrl);
                if (!singleSubscriber.isUnsubscribed()) {
                    singleSubscriber.onNext(res);
                    singleSubscriber.onCompleted();
                }
            } catch (Exception e) {
                if (!singleSubscriber.isUnsubscribed()) singleSubscriber.onError(e);
            }
        });
    }

    public Observable<UserRes> loginVk(VkDataDto vkDataDto) {
        return getVkProfile(AppConfig.VK_BASE_URL + "users.get?fields=photo_200,contacts", vkDataDto)
                .subscribeOn(Schedulers.io())
                .map(vkProfileRes -> new SocialLoginReq(vkProfileRes.getFirstName(), vkProfileRes.getLastName(), vkProfileRes.getAvatar(), vkDataDto.getEmail(), vkProfileRes.getPhone()))
                .flatMap(this::socialLogin)
                .doOnNext(userRes -> mPreferencesManager.saveProfileInfo(userRes));
    }

    public Observable<UserRes> loginFacebook(FacebookDataDto facebookDataDto) {
        return getFacebookProfile(AppConfig.FACEBOOK_BASE_URL + "me?fields=email,picture.width(200).height(200),first_name,last_name", facebookDataDto)
                .subscribeOn(Schedulers.io())
                .map(facebookProfileRes -> new SocialLoginReq(facebookProfileRes.getFirstName(), facebookProfileRes.getLastName(), facebookProfileRes.getAvatar(), facebookProfileRes.getEmail(), "не задан"))
                .flatMap(this::socialLogin)
                .doOnNext(userRes -> mPreferencesManager.saveProfileInfo(userRes));
    }

    public Observable<UserRes> loginTwitter(TwitterSession session) {
        return getTwitterProfile(session)
                .subscribeOn(Schedulers.io())
                .map(twitterProfileRes -> new SocialLoginReq(twitterProfileRes.getName(), twitterProfileRes.getAvatar(), twitterProfileRes.getEmail(), null))
                .flatMap(this::socialLogin)
                .doOnNext(userRes -> mPreferencesManager.saveProfileInfo(userRes));
    }

    //endregion

    //region ======================== Products ========================

    public Observable<ProductRealm> getProductsObsFromRealm() {
        return mRealmManager.getAllProductsFromRealm();
    }

    public Observable<ProductRealm> getProductsObsFromNetwork() {
        return mRestService.getProductResObs(mPreferencesManager.getLastProductUpdate())
                .compose((RestCallTransformer<List<ProductRes>>) mRestCallTransformer) // handle response
                .flatMap(Observable::from)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext(productRes -> {
                    if (!productRes.isActive()) {
                        mRealmManager.deleteFromRealm(ProductRealm.class, productRes.getId());
                    }
                })
                .filter(ProductRes::isActive)
                .doOnNext(productRes -> mRealmManager.saveProductResponseToRealm(productRes))
                .retryWhen(errorObservable -> errorObservable
                        .zipWith(Observable.range(1, AppConfig.RETRY_REQUEST_COUNT), (throwable, retryCount) -> retryCount)
                        .doOnNext(retryCount -> Log.e(TAG, "LOCAL UPDATE request retry count: " + retryCount + " " + new Date()))
                        .map(retryCount -> (long) (AppConfig.RETRY_REQUEST_BASE_DELAY * Math.pow(Math.E, retryCount)))
                        .doOnNext(delay -> Log.e(TAG, "LOCAL UPDATE delay: " + delay))
                        .flatMap(delay -> Observable.timer(delay, TimeUnit.MILLISECONDS))
                )
                .flatMap(productRes -> Observable.empty());
    }

    //endregion

    //region ======================== Comments ========================

    public Observable<CommentRes> sendComment(String productId, CommentRes comment) {
        return mRestService.sendComment(productId, comment);
    }

    //endregion

    //region ======================== Addresses ========================

    public Observable<AddressRealm> getAddressesObsFromRealm() {
        return mRealmManager.getAddressesFromRealm();
    }

    public AddressRealm getAddress(String addressId) {
        return mRealmManager.getAddressFromRealm(addressId);
    }

    public void updateOrInsertAddress(AddressRealm address) {
        mRealmManager.saveAddressToRealm(address);
    }

    public void deleteAddress(String addressId) {
        mRealmManager.deleteFromRealm(AddressRealm.class, addressId);
    }

    //endregion

    //region ======================== ProfileInfo ========================

    public ProfileInfoDto getProfileInfo() {
        return mPreferencesManager.getProfileInfo();
    }

    public void saveProfileInfo(ProfileInfoDto profileInfo) {
        mPreferencesManager.saveProfileInfo(profileInfo);
    }

    public String getUserName() {
        return mPreferencesManager.getUserName();
    }

    public Observable<AvatarUrlRes> uploadUserAvatar(MultipartBody.Part body) {
        return mRestService.uploadUserAvatar(body);
    }

    public void saveUserAvatar(String avatarUrl) {
        mPreferencesManager.saveUserAvatar(avatarUrl);
    }

    //endregion

    //region ======================== Settings ========================

    public UserSettingsDto getUserSettings() {
        return mPreferencesManager.getUserSettings();
    }

    public void saveSettings(UserSettingsDto settings) {
        mPreferencesManager.saveSettings(settings);
    }

    //endregion

    //region ======================== Notifications ========================

    public void saveNotification(NotificationRealm notification) {
        mRealmManager.saveNotification(notification);
    }

    public Observable<NotificationRealm> getNotificationObs() {
        return mRealmManager.getNotificationObs();
    }

    //endregion
}
