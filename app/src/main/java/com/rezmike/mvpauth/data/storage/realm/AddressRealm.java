package com.rezmike.mvpauth.data.storage.realm;

import android.support.annotation.Nullable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AddressRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private int floor;
    private String comment;

    public AddressRealm() {
    }

    public AddressRealm(@Nullable String id, String name, String street, String house, String apartment, int floor, String comment) {
        this.name = name;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
        this.floor = floor;
        this.comment = comment;
        this.id = id != null ? id : String.valueOf(this.hashCode());
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public int getFloor() {
        return floor;
    }

    public String getComment() {
        return comment;
    }
}
