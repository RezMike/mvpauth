package com.rezmike.mvpauth.data.network.res;

public class AvatarUrlRes {

    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
