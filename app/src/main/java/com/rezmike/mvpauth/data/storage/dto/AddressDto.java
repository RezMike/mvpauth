package com.rezmike.mvpauth.data.storage.dto;

import com.rezmike.mvpauth.data.storage.realm.AddressRealm;

public class AddressDto {
    private String id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private int floor;
    private String comment;

    public AddressDto(AddressRealm addressRealm) {
        this.id = addressRealm.getId();
        this.name = addressRealm.getName();
        this.street = addressRealm.getStreet();
        this.house = addressRealm.getHouse();
        this.apartment = addressRealm.getApartment();
        this.floor = addressRealm.getFloor();
        this.comment = addressRealm.getComment();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public int getFloor() {
        return floor;
    }

    public String getComment() {
        return comment;
    }
}
