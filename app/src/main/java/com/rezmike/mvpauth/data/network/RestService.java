package com.rezmike.mvpauth.data.network;

import com.rezmike.mvpauth.data.network.req.SocialLoginReq;
import com.rezmike.mvpauth.data.network.req.UserLoginReq;
import com.rezmike.mvpauth.data.network.res.AvatarUrlRes;
import com.rezmike.mvpauth.data.network.res.CommentRes;
import com.rezmike.mvpauth.data.network.res.FacebookProfileRes;
import com.rezmike.mvpauth.data.network.res.ProductRes;
import com.rezmike.mvpauth.data.network.res.UserRes;
import com.rezmike.mvpauth.data.network.res.VkProfileRes;
import com.rezmike.mvpauth.utils.ConstantManager;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

public interface RestService {

    @GET("products")
    Observable<Response<List<ProductRes>>> getProductResObs(@Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);

    @POST("products/{productId}/comments")
    Observable<CommentRes> sendComment(@Path("productId") String productId, @Body CommentRes commentRes);

    @Multipart
    @POST("avatar")
    Observable<AvatarUrlRes> uploadUserAvatar(@Part MultipartBody.Part file);

    @POST("auth/login")
    Observable<Response<UserRes>> loginUser(@Body UserLoginReq userLoginReq);

    @POST("auth/signup")
    Observable<Response<UserRes>> socialLogin(@Body SocialLoginReq userSocReq);

    @GET
    Observable<VkProfileRes> getVkProfile(@Url String baseVkApiUrl, @Query("access_token") String vkToken);

    @GET
    Observable<FacebookProfileRes> getFacebookProfile(@Url String baseUrl, @Query("access_token") String accessToken);
}
