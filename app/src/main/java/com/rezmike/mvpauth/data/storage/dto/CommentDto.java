package com.rezmike.mvpauth.data.storage.dto;

import com.rezmike.mvpauth.data.storage.realm.CommentRealm;

import java.util.Date;

public class CommentDto {
    private String userName;
    private String avatarUrl;
    private float rating;
    private Date commentDate;
    private String comment;

    public CommentDto(CommentRealm commentRealm) {
        this.userName = commentRealm.getUserName();
        this.avatarUrl = commentRealm.getAvatar();
        this.rating = commentRealm.getRating();
        this.commentDate = commentRealm.getCommentDate();
        this.comment = commentRealm.getComment();
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public float getRating() {
        return rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }
}
