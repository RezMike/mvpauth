package com.rezmike.mvpauth.data.managers;

import com.rezmike.mvpauth.data.network.res.CommentRes;
import com.rezmike.mvpauth.data.network.res.ProductRes;
import com.rezmike.mvpauth.data.storage.realm.AddressRealm;
import com.rezmike.mvpauth.data.storage.realm.CommentRealm;
import com.rezmike.mvpauth.data.storage.realm.NotificationRealm;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class RealmManager {

    private Realm mRealmInstance;

    //region ======================== Products ========================

    public void saveProductResponseToRealm(ProductRes productRes) {
        Realm realm = Realm.getDefaultInstance();

        ProductRealm productRealm = new ProductRealm(productRes);

        if (!productRes.getComments().isEmpty()) {
            Observable.from(productRes.getComments())
                    .doOnNext(commentRes -> {
                        if (!commentRes.isActive()) {
                            deleteFromRealm(CommentRealm.class, commentRes.getId());
                        }
                    })
                    .filter(CommentRes::isActive)
                    .map(CommentRealm::new)
                    .subscribe(commentRealm -> productRealm.getComments().add(commentRealm));
        }

        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(productRealm));
        realm.close();
    }

    public Observable<ProductRealm> getAllProductsFromRealm() {
        RealmResults<ProductRealm> managedProducts = getQueryRealmInstance().where(ProductRealm.class).findAllAsync();
        return managedProducts
                .asObservable()
                .filter(RealmResults::isLoaded) // hot observable
                //.first() // if need cold observable
                .flatMap(Observable::from);
    }

    //endregion

    //region ======================== Addresses ========================

    public Observable<AddressRealm> getAddressesFromRealm() {
        RealmResults<AddressRealm> managedAddresses = getQueryRealmInstance().where(AddressRealm.class).findAllAsync();
        return managedAddresses.asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    public AddressRealm getAddressFromRealm(String addressId) {
        return getQueryRealmInstance().where(AddressRealm.class)
                .equalTo("id", addressId)
                .findFirst();
    }

    public void saveAddressToRealm(AddressRealm address) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(address));
        realm.close();
    }

    //endregion

    //region ======================== Notifications ========================

    public Observable<NotificationRealm> getNotificationObs() {
        // TODO: 04.05.2017 hot observable, order notification
        RealmResults<NotificationRealm> managedNotifications = getQueryRealmInstance().where(NotificationRealm.class).findAllAsync();
        return managedNotifications.asObservable()
                .subscribeOn(AndroidSchedulers.mainThread())
                .filter(RealmResults::isLoaded)
                .first()
                .flatMap(Observable::from);
    }

    public void saveNotification(NotificationRealm notification) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(notification));
        realm.close();
    }

    //endregion

    public void deleteFromRealm(Class<? extends RealmObject> entityRealmClass, String id) {
        Realm realm = Realm.getDefaultInstance();
        RealmObject entity = realm.where(entityRealmClass).equalTo("id", id).findFirst();

        if (entity != null) {
            realm.executeTransaction(realm1 -> entity.deleteFromRealm());
        }
        realm.close();
    }

    private Realm getQueryRealmInstance() {
        if (mRealmInstance == null || mRealmInstance.isClosed()) {
            mRealmInstance = Realm.getDefaultInstance();
        }
        return mRealmInstance;
    }
}
