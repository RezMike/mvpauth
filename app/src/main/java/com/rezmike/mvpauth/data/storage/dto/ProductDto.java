package com.rezmike.mvpauth.data.storage.dto;

import com.rezmike.mvpauth.data.storage.realm.ProductRealm;

public class ProductDto {
    private String id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private int count;
    private boolean favorite;

    public ProductDto(ProductRealm productRealm) {
        this.productName = productRealm.getProductName();
        this.imageUrl = productRealm.getImageUrl();
        this.description = productRealm.getDescription();
        this.price = productRealm.getPrice();
        this.count = productRealm.getCount();
        this.favorite = productRealm.isFavorite();
    }

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public boolean isFavorite() {
        return favorite;
    }
}
