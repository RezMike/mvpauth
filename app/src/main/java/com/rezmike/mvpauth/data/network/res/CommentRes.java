package com.rezmike.mvpauth.data.network.res;

import com.rezmike.mvpauth.data.storage.realm.CommentRealm;
import com.squareup.moshi.Json;

import java.util.Date;

public class CommentRes {

    @Json(name = "_id")
    private String id;
    private String avatar;
    private String userName;
    @Json(name = "raiting")
    private float rating;
    private String comment;
    private Date commentDate;
    private boolean active;

    public CommentRes(CommentRealm comment) {
        this.avatar = comment.getAvatar();
        this.userName = comment.getUserName();
        this.rating = comment.getRating();
        this.comment = comment.getComment();
        this.commentDate = comment.getCommentDate();
    }

    public String getId() {
        return id;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getUserName() {
        return userName;
    }

    public float getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public boolean isActive() {
        return active;
    }
}