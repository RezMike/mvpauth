package com.rezmike.mvpauth.data.network.req;

public class SocialLoginReq {
    private String fullName;
    private String avatarUrl;
    private String email;
    private String phone;

    public SocialLoginReq(String firstName, String lastName, String avatarUrl, String email, String phone) {
        this.fullName = lastName + " " + firstName;
        this.avatarUrl = avatarUrl;
        this.email = email;
        this.phone = phone;
    }

    public SocialLoginReq(String fullName, String avatarUrl, String email, String phone) {
        this.fullName = fullName;
        this.avatarUrl = avatarUrl;
        this.email = email;
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
