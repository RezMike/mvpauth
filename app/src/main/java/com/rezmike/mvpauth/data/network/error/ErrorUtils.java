package com.rezmike.mvpauth.data.network.error;

import retrofit2.Response;

public class ErrorUtils {

    public static ApiError parseError(Response<?> response) {
        // TODO: 06.04.2017 parse error correctly
        return new ApiError(response.code());
    }
}
