package com.rezmike.mvpauth.data.network;

import android.support.annotation.VisibleForTesting;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.data.network.error.AccessError;
import com.rezmike.mvpauth.data.network.error.ErrorUtils;
import com.rezmike.mvpauth.data.network.error.NetworkAvailableError;
import com.rezmike.mvpauth.utils.ConstantManager;
import com.rezmike.mvpauth.utils.NetworkStatusChecker;

import retrofit2.Response;
import rx.Observable;

import static android.support.annotation.VisibleForTesting.NONE;

public class RestCallTransformer<R> implements Observable.Transformer<Response<R>, R> {

    private boolean isInTestMode;

    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
        Observable<Boolean> networkStatus;
        if (isInTestMode) {
            networkStatus = Observable.just(true);
        } else {
            networkStatus = NetworkStatusChecker.isInternetAvailable();
        }
        return networkStatus
                .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case 200:
                            String lastModified = rResponse.headers().get(ConstantManager.LAST_MODIFIED_HEADER);
                            if (lastModified != null) {
                                DataManager.getInstance().getPreferencesManager().saveLastProductUpdate(lastModified);
                            }
                            return Observable.just(rResponse.body());
                        case 304:
                            return Observable.empty();
                        case 403:
                            return Observable.error(new AccessError());
                        default:
                            return Observable.error(ErrorUtils.parseError(rResponse));
                    }
                });
    }

    @VisibleForTesting(otherwise = NONE)
    public void setTestMode() {
        isInTestMode = true;
    }
}
