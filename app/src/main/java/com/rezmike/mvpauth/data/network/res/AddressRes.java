package com.rezmike.mvpauth.data.network.res;

import android.support.annotation.VisibleForTesting;

import com.squareup.moshi.Json;

import static android.support.annotation.VisibleForTesting.NONE;

public class AddressRes {

    @Json(name = "_id")
    private String id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private int floor;
    private String comment;

    @VisibleForTesting(otherwise = NONE)
    public AddressRes(String id, String name, String street, String house, String apartment, int floor, String comment) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
        this.floor = floor;
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public int getFloor() {
        return floor;
    }

    public String getComment() {
        return comment;
    }
}
