package com.rezmike.mvpauth.data.network.res;

import android.support.annotation.Nullable;

public class TwitterProfileRes {
    private String name;
    @Nullable
    private String email;
    private String avatar;

    public TwitterProfileRes(String name, @Nullable String email, String avatar) {
        this.name = name;
        this.email = email;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public String getAvatar() {
        return avatar;
    }
}
