package com.rezmike.mvpauth.data.network.res;

import com.squareup.moshi.Json;

import java.util.List;

public class VkProfileRes {

    private List<Response> response = null;

    public String getFullName() {
        return response.get(0).lastName + " " + response.get(0).firstName;
    }

    public String getFirstName() {
        return response.get(0).firstName;
    }

    public String getLastName() {
        return response.get(0).lastName;
    }

    public String getAvatar() {
        return response.get(0).photo;
    }

    public String getPhone() {
        return response.get(0).mobilePhone;
    }

    private static class Response {
        @Json(name = "uid")
        int id;
        @Json(name = "first_name")
        String firstName;
        @Json(name = "last_name")
        String lastName;
        @Json(name = "photo_200")
        String photo;
        @Json(name = "mobile_phone")
        String mobilePhone;
        @Json(name = "home_phone")
        String homePhone;
    }
}