package com.rezmike.mvpauth.data.storage.dto;

public class ProfileInfoDto {
    private String name;
    private String phone;
    private String avatar;

    public ProfileInfoDto(String name, String phone, String avatar) {
        this.name = name;
        this.phone = phone;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAvatar() {
        return avatar;
    }
}
