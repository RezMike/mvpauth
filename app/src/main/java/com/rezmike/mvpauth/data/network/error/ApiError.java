package com.rezmike.mvpauth.data.network.error;

public class ApiError extends Throwable {

    private int statusCode;

    public ApiError(String message) {
        super(message);
    }

    public ApiError(int statusCode) {
        super("status code: " + statusCode);
        this.statusCode = statusCode;
    }

    public int getStatus() {
        return statusCode;
    }
}
