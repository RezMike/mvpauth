package com.rezmike.mvpauth.data.storage.dto;

public class FacebookDataDto {
    private String accessToken;
    private String userId;

    public FacebookDataDto(String accessToken, String userId) {
        this.accessToken = accessToken;
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getUserId() {
        return userId;
    }
}
