package com.rezmike.mvpauth.utils;

public class ConstantManager {

    public static final int REQUEST_PERMISSION_CAMERA = 100;
    public static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 101;

    public static final int REQUEST_PROFILE_PHOTO_GALLERY = 200;
    public static final int REQUEST_PROFILE_PHOTO_CAMERA = 201;

    public static final String FILE_PROVIDER_AUTHORITY = "com.rezmike.mvpauth.fileprovider";
    public static final String LAST_MODIFIED_HEADER = "Last-Modified";
    public static final String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";

    public static final String NOTIFICATION_ORDER_TYPE = "ORDER";
    public static final String NOTIFICATION_PROMO_TYPE = "PROMO";

    public static final int NOTIFICATION_MANAGER_ID = 600;

    public static final int PUSH_REQ_CODE = 545;
}
