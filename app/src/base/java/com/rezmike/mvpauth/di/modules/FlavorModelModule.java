package com.rezmike.mvpauth.di.modules;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.rezmike.mvpauth.MvpApplication;
import com.rezmike.mvpauth.utils.AppConfig;

import dagger.Module;
import dagger.Provides;

@Module
public class FlavorModelModule {

    @Provides
    JobManager provideJobManager() {

        Configuration configuration = new Configuration.Builder(MvpApplication.getContext())
                .minConsumerCount(AppConfig.JOB_MIN_CONSUMER_COUNT) // always keep at least one consumer alive
                .maxConsumerCount(AppConfig.JOB_MAX_CONSUMER_COUNT) // up to 3 consumer at a time
                .loadFactor(AppConfig.JOB_LOAD_FACTOR) // 3 jobs per consumer
                .consumerKeepAlive(AppConfig.JOB_KEEP_ALIVE) // wait a minute for thread
                .build();

        return new JobManager(configuration);
    }
}
