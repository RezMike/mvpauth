package com.rezmike.mvpauth.ui.screens.auth;

import android.support.design.widget.TextInputLayout;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.ui.activities.RootActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressKey;
import static android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.view.KeyEvent.KEYCODE_ENTER;
import static org.hamcrest.CoreMatchers.not;

public class AuthViewTest {

    @Rule
    public ActivityTestRule<RootActivity> mActivityTestRule = new ActivityTestRule<>(RootActivity.class);

    private static final String TEST_EMAIL_VALID = "email@mail.ru";
    private static final String TEST_EMAIL_INVALID = "email@mail";
    private static final String TEST_PASSWORD_VALID = "AnyPassword";
    private static final String TEST_PASSWORD_INVALID = "pass";
    private static final String TEST_EMAIL_ERROR = "Введен неверный email";
    private static final String TEST_PASSWORD_ERROR = "Длина пароля - не менее 8 символов";
    private static final String NO_ERROR = "";

    private ViewInteraction mLoginBtn;
    private ViewInteraction mShowCatalogBtn;
    private ViewInteraction mEmailInput;
    private ViewInteraction mEmailWrapper;
    private ViewInteraction mPasswordInput;
    private ViewInteraction mPasswordWrapper;

    @Before
    public void setup() {
        //mLoginBtn = onView(Matchers.allOf(withId(R.id.login_btn), withText(R.string.auth_login)));
        mLoginBtn = onView(withId(R.id.login_btn));
        mShowCatalogBtn = onView(withId(R.id.show_catalog_btn));
        mEmailInput = onView(withId(R.id.login_email_et));
        mEmailWrapper = onView(withId(R.id.login_email_wrap));
        mPasswordInput = onView(withId(R.id.login_password_et));
        mPasswordWrapper = onView(withId(R.id.login_password_wrap));
    }

    @Test
    public void click_on_login_HIDE_SHOW_CATALOG_BTN() throws Exception {
        mLoginBtn.perform(click());
        mShowCatalogBtn.check(matches(not(isDisplayed())));
    }

    @Test
    public void back_pressed_LOGIN_CARD_NOT_DISPLAYED() throws Exception {
        mLoginBtn.perform(click());
        pressBack();
        mShowCatalogBtn.check(matches(isDisplayed()));
    }

    @Test
    public void input_valid_mail_password_NO_ERRORS() throws Exception {
        mLoginBtn.perform(click());
        mEmailInput.perform(click());
        mEmailInput.check(matches(hasFocus()));

        mEmailInput.perform(typeTextIntoFocusedView(TEST_EMAIL_VALID));
        mEmailInput.check(matches(withText(TEST_EMAIL_VALID)));
        mEmailWrapper.check(matches(layoutWithError(NO_ERROR)));

        mEmailInput.perform(pressKey(KEYCODE_ENTER));
        mPasswordInput.check(matches(hasFocus()));

        mPasswordInput.perform(typeTextIntoFocusedView(TEST_PASSWORD_VALID));
        mPasswordInput.check(matches(withText(TEST_PASSWORD_VALID)));
        mPasswordWrapper.check(matches(layoutWithError(NO_ERROR)));
    }

    @Test
    public void input_invalid_mail_password_ERRORS() throws Exception {
        mLoginBtn.perform(click());
        mEmailInput.perform(click());
        mEmailInput.check(matches(hasFocus()));

        mEmailInput.perform(typeTextIntoFocusedView(TEST_EMAIL_INVALID));
        mEmailInput.check(matches(withText(TEST_EMAIL_INVALID)));
        mEmailWrapper.check(matches(layoutWithError(TEST_EMAIL_ERROR)));

        mEmailInput.perform(pressKey(KEYCODE_ENTER));
        mPasswordInput.check(matches(hasFocus()));

        mPasswordInput.perform(typeTextIntoFocusedView(TEST_PASSWORD_INVALID));
        mPasswordInput.check(matches(withText(TEST_PASSWORD_INVALID)));
        mPasswordWrapper.check(matches(layoutWithError(TEST_PASSWORD_ERROR)));
    }

    public static Matcher<View> layoutWithError(final String expectedError) {
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {

            }

            @Override
            protected boolean matchesSafely(View item) {
                if (!(item instanceof TextInputLayout)) return false;

                TextInputLayout layout = (TextInputLayout) item;

                CharSequence error = layout.getError();
                if (error == null) {
                    return expectedError.equals(NO_ERROR);
                } else {
                    return expectedError.equals(error.toString());
                }
            }
        };
    }
}