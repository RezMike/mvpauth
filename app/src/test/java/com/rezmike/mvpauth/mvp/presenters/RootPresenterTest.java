package com.rezmike.mvpauth.mvp.presenters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.view.ViewPager;

import com.rezmike.mvpauth.data.storage.dto.ActivityResultDto;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.components.AppComponent;
import com.rezmike.mvpauth.di.components.DaggerAppComponent;
import com.rezmike.mvpauth.di.modules.AppModule;
import com.rezmike.mvpauth.di.modules.RootModule;
import com.rezmike.mvpauth.mvp.models.AccountModel;
import com.rezmike.mvpauth.mvp.views.IActionBarBuilder;
import com.rezmike.mvpauth.mvp.views.IRootView;
import com.rezmike.mvpauth.resources.StubEntityFactory;
import com.rezmike.mvpauth.ui.activities.DaggerRootActivity_RootComponent;
import com.rezmike.mvpauth.ui.activities.RootActivity;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

public class RootPresenterTest {

    @Mock
    private AccountModel mAccountModel;
    @Mock
    private RootActivity mRootView;
    @Mock
    private Context mContext;

    private RootPresenter mPresenter;
    private BundleServiceRunner mBundleServiceRunner;
    private MortarScope mMortarScope;
    private RootActivity.RootComponent mTestRootComponent;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        prepareDependency();
        prepareScope();
        prepareRxSchedulers();
        prepareStubs();

        mPresenter = new RootPresenter();
    }

    //region ======================== Prepare ========================

    private void prepareDependency() {
        AppComponent testAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mContext))
                .build();
        mTestRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(testAppComponent)
                .rootModule(new RootModule() {
                    @Override
                    public RootPresenter provideRootPresenter() {
                        return mock(RootPresenter.class);
                    }

                    @Override
                    public AccountModel provideAccountModel() {
                        return mAccountModel;
                    }
                })
                .build();
    }

    private void prepareScope() {
        mBundleServiceRunner = new BundleServiceRunner();
        mMortarScope = MortarScope.buildRootScope()
                .withService(BundleServiceRunner.SERVICE_NAME, mBundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, mTestRootComponent)
                .build("MockRoot");
    }

    private void prepareRxSchedulers() {
        RxAndroidPlugins.getInstance().reset();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
    }

    private void prepareStubs() {
        //noinspection ResourceType
        given(mRootView.getSystemService(BundleServiceRunner.SERVICE_NAME)).willReturn(mBundleServiceRunner);
        //noinspection ResourceType
        given(mRootView.getSystemService(MortarScope.class.getName())).willReturn(mMortarScope);
        //noinspection ResourceType
        given(mAccountModel.getProfileInfoSub()).willReturn(BehaviorSubject.create());
    }

    //endregion

    @After
    public void tearDown() throws Exception {
        mPresenter.dropView(mRootView);
    }

    @Test
    public void getRootView_takeView_INSTANCE_OF_ROOT_ACTIVITY() throws Exception {
        //given
        mPresenter.takeView(mRootView);

        //when
        IRootView actualView = mPresenter.getRootView();

        //then
        assertTrue(actualView instanceof RootActivity);
    }

    @Test
    public void newActionBarBuilder_call_NEW_BUILDER_OBJECT() throws Exception {
        //given

        //when
        IActionBarBuilder actualBuilder1 = mPresenter.newActionBarBuilder();
        IActionBarBuilder actualBuilder2 = mPresenter.newActionBarBuilder();

        //then
        assertNotEquals(actualBuilder1, actualBuilder2);
    }

    @Test
    public void ActionBarBuilder_buildWithTabs_CALL_ACTIVITY_SET_TABLAYOUT() throws Exception {
        //given
        boolean expectedVisable = true;
        String expectedTitle = "title";
        boolean expectedArrow = true;
        MenuItemHolder expectedAction1 = StubEntityFactory.makeStub(MenuItemHolder.class);
        MenuItemHolder expectedAction2 = StubEntityFactory.makeStub(MenuItemHolder.class);
        List<MenuItemHolder> expectedActions = new ArrayList<>();
        expectedActions.add(expectedAction1);
        expectedActions.add(expectedAction2);
        ViewPager mockTab = mock(ViewPager.class);

        mPresenter.takeView(mRootView);

        //when
        IActionBarBuilder actualBuilder = mPresenter.newActionBarBuilder();
        actualBuilder.setVisable(expectedVisable)
                .setActionBarTitle(expectedTitle)
                .setBackArrow(expectedArrow)
                .addAction(expectedAction1)
                .addAction(expectedAction2)
                .setTab(mockTab)
                .build();

        //then
        then(mRootView).should(times(1)).setActionBarVisable(expectedVisable);
        then(mRootView).should(times(1)).setActionBarTitle(expectedTitle);
        then(mRootView).should(times(1)).setBackArrow(expectedArrow);
        then(mRootView).should(times(1)).setMenuItems(expectedActions);
        then(mRootView).should(times(1)).setTabLayout(mockTab);
    }

    @Test
    public void ActionBarBuilder_buildWithoutTabs_CALL_ACTIVITY_REMOVE_TABLAYOUT() throws Exception {
        //given
        boolean expectedVisable = false;
        String expectedTitle = "title2";
        boolean expectedArrow = false;
        MenuItemHolder expectedAction1 = StubEntityFactory.makeStub(MenuItemHolder.class);
        List<MenuItemHolder> expectedActions = new ArrayList<>();
        expectedActions.add(expectedAction1);

        mPresenter.takeView(mRootView);

        //when
        IActionBarBuilder actualBuilder = mPresenter.newActionBarBuilder();
        actualBuilder.setVisable(expectedVisable)
                .setActionBarTitle(expectedTitle)
                .setBackArrow(expectedArrow)
                .addAction(expectedAction1)
                .build();

        //then
        then(mRootView).should(times(1)).setActionBarVisable(expectedVisable);
        then(mRootView).should(times(1)).setActionBarTitle(expectedTitle);
        then(mRootView).should(times(1)).setBackArrow(expectedArrow);
        then(mRootView).should(times(1)).setMenuItems(expectedActions);
        then(mRootView).should(times(1)).removeTabLayout();
    }

    @Test
    public void onActivityResult_stubActivityResult_NOT_COMPLETED_OBS() throws Exception {
        //given
        int expectedRequestCode = 1;
        int expectedResultCode = Activity.RESULT_OK;
        Intent expectedIntent = mock(Intent.class);
        TestSubscriber<ActivityResultDto> subscriber = new TestSubscriber<>();

        mPresenter.takeView(mRootView);

        //when
        mPresenter.getActivityResultSubject().subscribe(subscriber);
        mPresenter.onActivityResult(expectedRequestCode, expectedResultCode, expectedIntent);

        //then
        subscriber.assertNotCompleted();
        subscriber.assertNoErrors();

        ActivityResultDto actualActivityResult = subscriber.getOnNextEvents().get(0);
        assertEquals(expectedRequestCode, actualActivityResult.getRequestCode());
        assertEquals(expectedResultCode, actualActivityResult.getResultCode());
        assertEquals(expectedIntent, actualActivityResult.getIntent());
    }

    @Test
    public void checkPermissionAndRequestIfNotGranted_permissionsGranted_NOT_REQUEST_PERMISSION() throws Exception {
        //given
        int expectedRequestCode = 1;
        String[] expectedPermissions = new String[]{
                "android.permisson.WRITE_EXTERNAL_STORAGE",
                "android.permisson.READ_EXTERNAL_STORAGE"
        };
        mPresenter.takeView(mRootView);
        given(mRootView.isAllGranted(expectedPermissions, false)).willReturn(true);

        //when
        mPresenter.checkPermissionAndRequestIfNotGranted(expectedPermissions, expectedRequestCode);

        //then
        then(mRootView).should(never()).requestPermissions(expectedPermissions, expectedRequestCode);
    }

    @Test
    public void checkPermissionAndRequestIfNotGranted_permissionsDenied_sdk23_REQUEST_PERMISSION() throws Exception {
        //given
        int expectedRequestCode = 1;
        String[] expectedPermissions = new String[]{
                "android.permisson.WRITE_EXTERNAL_STORAGE",
                "android.permisson.READ_EXTERNAL_STORAGE"
        };
        mPresenter.takeView(mRootView);
        given(mRootView.isAllGranted(expectedPermissions, false)).willReturn(false);
        setFinalStatic(Build.VERSION.class.getField("SDK_INT"), 23);

        //when
        mPresenter.checkPermissionAndRequestIfNotGranted(expectedPermissions, expectedRequestCode);

        //then
        then(mRootView).should(times(1)).requestPermissions(expectedPermissions, expectedRequestCode);
    }

    @Test
    public void checkPermissionAndRequestIfNotGranted_permissionsDenied_sdk19_NOT_REQUEST_PERMISSION() throws Exception {
        //given
        int expectedRequestCode = 1;
        String[] expectedPermissions = new String[]{
                "android.permisson.WRITE_EXTERNAL_STORAGE",
                "android.permisson.READ_EXTERNAL_STORAGE"
        };
        mPresenter.takeView(mRootView);
        given(mRootView.isAllGranted(expectedPermissions, false)).willReturn(false);
        setFinalStatic(Build.VERSION.class.getField("SDK_INT"), 19);

        //when
        mPresenter.checkPermissionAndRequestIfNotGranted(expectedPermissions, expectedRequestCode);

        //then
        then(mRootView).should(never()).requestPermissions(expectedPermissions, expectedRequestCode);
    }

    static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiedField = Field.class.getDeclaredField("modifiers");
        modifiedField.setAccessible(true);
        modifiedField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, newValue);
    }
}