package com.rezmike.mvpauth.mvp.models;

import com.birbit.android.jobqueue.JobManager;
import com.rezmike.mvpauth.data.managers.DataManager;
import com.rezmike.mvpauth.data.network.req.UserLoginReq;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;

public class AuthModelTest {

    @Mock
    DataManager mockDataManager;

    @Mock
    JobManager mockJobManager;

    private AuthModel mModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mModel = new AuthModel(mockDataManager, mockJobManager);
    }

    @Test
    public void loginUser() throws Exception {
        mModel.loginUser("email@mail.ru", "password");
        verify(mockDataManager, only()).loginUser(any(UserLoginReq.class));
    }

    @Test
    public void isUserAuth() throws Exception {
        mModel.isUserAuth();
        verify(mockDataManager, only()).isUserAuth();
    }

}