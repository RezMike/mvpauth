package com.rezmike.mvpauth.ui.screens.auth;

import android.content.Context;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.network.res.UserRes;
import com.rezmike.mvpauth.di.DaggerService;
import com.rezmike.mvpauth.di.components.AppComponent;
import com.rezmike.mvpauth.di.components.DaggerAppComponent;
import com.rezmike.mvpauth.di.modules.AppModule;
import com.rezmike.mvpauth.di.modules.RootModule;
import com.rezmike.mvpauth.mvp.models.AccountModel;
import com.rezmike.mvpauth.mvp.models.AuthModel;
import com.rezmike.mvpauth.mvp.presenters.IAuthPresenter;
import com.rezmike.mvpauth.mvp.presenters.RootPresenter;
import com.rezmike.mvpauth.mvp.views.IActionBarBuilder;
import com.rezmike.mvpauth.mvp.views.IFabBuilder;
import com.rezmike.mvpauth.resources.StubEntityFactory;
import com.rezmike.mvpauth.ui.activities.DaggerRootActivity_RootComponent;
import com.rezmike.mvpauth.ui.activities.RootActivity;
import com.rezmike.mvpauth.ui.screens.catalog.CatalogScreen;
import com.vk.sdk.VKAccessToken;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class AuthPresenterTest {

    @Mock
    private AccountModel mAccountModel;
    @Mock
    private RootPresenter mRootPresenter;

    @Mock
    private AuthView mView;
    @Mock
    private AuthModel mModel;
    @Mock
    private Context mContext;

    @Mock
    private RootActivity mRootView;
    @Mock
    private Flow mFlow;

    @Mock(answer = Answers.RETURNS_SELF)
    private IActionBarBuilder mActionBarBuilder;
    @Mock(answer = Answers.RETURNS_SELF)
    private IFabBuilder mFabBuilder;

    private AuthScreen.AuthPresenter mPresenter;
    private BundleServiceRunner mBundleServiceRunner;
    private MortarScope mMortarScope;
    private AuthScreen.Component mTestAuthComponent;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        prepareDependency();
        prepareScope();
        prepareRxSchedulers();
        prepareStubs();

        mPresenter = new AuthScreen.AuthPresenter(mock(LoginManager.class), mock(CallbackManager.class));
        mPresenter.testSocial();
    }

    //region ======================== Prepare ========================

    private void prepareDependency() {
        AppComponent testAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mContext))
                .build();
        RootActivity.RootComponent testRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(testAppComponent)
                .rootModule(new RootModule() {
                    @Override
                    public RootPresenter provideRootPresenter() {
                        return mRootPresenter;
                    }

                    @Override
                    public AccountModel provideAccountModel() {
                        return mAccountModel;
                    }
                })
                .build();

        mTestAuthComponent = DaggerAuthScreen_Component.builder()
                .rootComponent(testRootComponent)
                .module(new AuthScreen.Module() {
                    @Override
                    AuthScreen.AuthPresenter providePresenter() {
                        return mock(AuthScreen.AuthPresenter.class);
                    }

                    @Override
                    AuthModel provideAuthModel() {
                        return mModel;
                    }
                })
                .build();
    }

    private void prepareScope() {
        mBundleServiceRunner = new BundleServiceRunner();
        mMortarScope = MortarScope.buildRootScope()
                .withService(BundleServiceRunner.SERVICE_NAME, mBundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, mTestAuthComponent)
                .build("MockRoot");
    }

    private void prepareRxSchedulers() {
        RxAndroidPlugins.getInstance().reset();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
    }

    private void prepareStubs() {
        //noinspection ResourceType
        given(mContext.getSystemService(BundleServiceRunner.SERVICE_NAME)).willReturn(mBundleServiceRunner);
        //noinspection ResourceType
        given(mContext.getSystemService(MortarScope.class.getName())).willReturn(mMortarScope);
        //noinspection ResourceType
        given(mContext.getSystemService("flow.InternalContextWrapper.FLOW_SERVICE")).willReturn(mFlow);

        given(mRootPresenter.getRootView()).willReturn(mRootView);
        given(mRootPresenter.newActionBarBuilder()).willReturn(mActionBarBuilder);
        given(mRootPresenter.newFabBuilder()).willReturn(mFabBuilder);
        given(mModel.isUserAuth()).willReturn(false);
        given(mView.getContext()).willReturn(mContext);
        given(mContext.getString(anyInt())).willReturn("");
    }

    //endregion

    @Test
    public void onLoad_isUserAuth_HIDE_LOGIN_BTN() throws Exception {
        //given
        given(mModel.isUserAuth()).willReturn(true);

        //when
        mPresenter.takeView(mView);

        //then
        then(mRootView).should(never()).showError(any(Throwable.class));
        then(mView).should(atMost(1)).hideLoginBtn();
    }

    @Test
    public void onLoad_notUserAuth_SHOW_LOGIN_BTN() throws Exception {
        //given
        given(mModel.isUserAuth()).willReturn(false);

        //when
        mPresenter.takeView(mView);

        //then
        then(mRootView).should(never()).showError(any(Throwable.class));
        then(mView).should(atMost(1)).showLoginBtn();
    }

    @Test
    public void initActionBar_splashTarget_SUCCESS_BUILD() throws Exception {
        //given
        given(mView.isSplashTarget()).willReturn(true);
        boolean expectedVisable = false;
        given(mRootPresenter.newActionBarBuilder()).willCallRealMethod();

        //when
        mPresenter.takeView(mView);

        //then
        then(mRootView).should().setActionBarVisable(expectedVisable);
    }

    @Test
    public void initActionBar_loginTarget_SUCCESS_BUILD() throws Exception {
        //given
        boolean expectedVisable = true;
        String expectedTitle = "Вход";
        boolean expectedArrow = true;
        given(mView.isSplashTarget()).willReturn(false);
        given(mView.getContext().getString(R.string.auth_title)).willReturn(expectedTitle);
        given(mRootPresenter.newActionBarBuilder()).willCallRealMethod();

        //when
        mPresenter.takeView(mView);

        //then
        then(mRootPresenter).should().newActionBarBuilder();
        then(mRootView).should().setActionBarVisable(expectedVisable);
        then(mRootView).should().setActionBarTitle(expectedTitle);
        then(mRootView).should().setBackArrow(expectedArrow);
    }

    @Test
    public void clickOnLogin_isIdle_SHOW_LOGIN_WITH_ANIM() throws Exception {
        //given
        given(mView.isIdle()).willReturn(true);

        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnLogin();

        //then
        then(mView).should(times(1)).showLoginWithAnim();
    }

    @Test
    public void clickOnLogin_notIdle_LOGIN_USER() throws Exception {
        //given
        String expectedEmail = "email@mail.ru";
        String expectedPassword = "password";
        given(mView.isIdle()).willReturn(false);
        given(mView.getUserEmail()).willReturn(expectedEmail);
        given(mView.getUserPassword()).willReturn(expectedPassword);
        given(mModel.loginUser(expectedEmail, expectedPassword)).willReturn(Observable.empty());

        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnLogin();

        //then
        then(mModel).should(times(1)).loginUser(expectedEmail, expectedPassword);
        then(mView).should(times(1)).hideLoginBtn();
        then(mView).should(times(1)).showIdleWithAnim();
    }

    @Test
    public void clickOnVk_notAuth_SUCCESS_USER_RES() throws Exception {
        //given
        String expectedUserName = "Резниченко Михаил";
        UserRes expectedUserRes = StubEntityFactory.makeStub(UserRes.class);
        given(mModel.loginVk(anyString(), anyString(), anyString())).willReturn(Observable.just(expectedUserRes));
        VKAccessToken mockVkToken = mock(VKAccessToken.class);
        mockVkToken.accessToken = "anyToken";
        mockVkToken.userId = "anyUserId";
        mockVkToken.email = "anyEmail";

        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnVk();
        mPresenter.onSocialResult(mockVkToken, IAuthPresenter.SocialSdkType.VK);

        //then
        then(mRootView).should().showMessage("Вы успешно авторизованы как " + expectedUserName);
        then(mRootView).should(never()).showError(any(Throwable.class));
        then(mRootPresenter).should().updateUserInfo();
    }

    @Test
    public void clickOnVk_alreadyAuth_SHOW_YOU_AUTH() throws Exception {
        //given
        String expectedUserName = "Резниченко Михаил";
        given(mModel.isUserAuth()).willReturn(true);
        given(mModel.getUserName()).willReturn(expectedUserName);

        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnVk();

        //then
        then(mRootView).should().showMessage("Вы уже авторизованы как " + expectedUserName);
        then(mRootView).should(never()).showError(any(Throwable.class));
    }

    @Test
    public void clickOnVk_cancelLogin_AUTH_CANCELED() throws Exception {
        //given
        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnVk();
        mPresenter.onSocialCancel();

        //then
        then(mRootView).should().showMessage("Авторизация отменена");
        then(mRootView).should(never()).showError(any(Throwable.class));
    }

    @Test
    public void clickOnFacebook_notAuth_SUCCESS_USER_RES() throws Exception {
        //given
        String expectedUserName = "Резниченко Михаил";
        UserRes expectedUserRes = StubEntityFactory.makeStub(UserRes.class);
        given(mModel.loginFacebook(anyString(), anyString())).willReturn(Observable.just(expectedUserRes));
        LoginResult mockLoginResult = mock(LoginResult.class, RETURNS_DEEP_STUBS);
        given(mockLoginResult.getAccessToken().getToken()).willReturn("anyToken");
        given(mockLoginResult.getAccessToken().getUserId()).willReturn("anyUserId");

        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnFacebook();
        mPresenter.onSocialResult(mockLoginResult, IAuthPresenter.SocialSdkType.FACEBOOK);

        //then
        then(mRootView).should().showMessage("Вы успешно авторизованы как " + expectedUserName);
        then(mRootView).should(never()).showError(any(Throwable.class));
        then(mRootPresenter).should().updateUserInfo();
    }

    @Test
    public void clickOnFacebook_alreadyAuth_SHOW_YOU_AUTH() throws Exception {
        //given
        String expectedUserName = "Резниченко Михаил";
        given(mModel.isUserAuth()).willReturn(true);
        given(mModel.getUserName()).willReturn(expectedUserName);

        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnFacebook();

        //then
        then(mRootView).should().showMessage("Вы уже авторизованы как " + expectedUserName);
        then(mRootView).should(never()).showError(any(Throwable.class));
    }

    @Test
    public void clickOnFacebook_cancelLogin_AUTH_CANCELED() throws Exception {
        //given
        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnFacebook();
        mPresenter.onSocialCancel();

        //then
        then(mRootView).should().showMessage("Авторизация отменена");
        then(mRootView).should(never()).showError(any(Throwable.class));
    }

    @Test
    public void clickOnTwitter() throws Exception {
        mPresenter.takeView(mView);
        mPresenter.clickOnTwitter();
        verify(mView).animateTwitterBtn();
    }

    @Test
    public void clickOnShowCatalog_isUserAuth_REPLACE_CATALOG_SCREEN() throws Exception {
        //given
        given(mModel.isUserAuth()).willReturn(true);

        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnShowCatalog();

        //then
        then(mFlow).should(times(1)).replaceHistory(any(CatalogScreen.class), any());
    }

    @Test
    public void clickOnShowCatalog_notUserAuth_SET_CATALOG_SCREEN() throws Exception {
        //given
        given(mModel.isUserAuth()).willReturn(false);

        mPresenter.takeView(mView);

        //when
        mPresenter.clickOnShowCatalog();

        //then
        then(mFlow).should(times(1)).set(any(CatalogScreen.class));
    }

    @Test
    public void checkUserAuth() throws Exception {
        //given
        mPresenter.takeView(mView);

        //when
        mPresenter.checkUserAuth();

        //then
        then(mModel).should(times(2)).isUserAuth();
    }

    @Test
    public void checkEmailValid_true_SET_EMAIL_ERROR_0() {
        //given
        String testEmail = "email@mail.ru";
        int expectedError = 0;

        mPresenter.takeView(mView);

        //when
        mPresenter.checkEmailValid(testEmail);

        //then
        then(mView).should().setEmailError(expectedError);
    }

    @Test
    public void checkEmailValid_false_SET_EMAIL_ERROR_ANY_STRING() {
        //given
        String testEmail1 = "email";
        String testEmail2 = "email@mail";
        String testEmail3 = "@mail.ru";

        mPresenter.takeView(mView);

        //when
        mPresenter.checkEmailValid(testEmail1);
        mPresenter.checkEmailValid(testEmail2);
        mPresenter.checkEmailValid(testEmail3);

        //then
        then(mView).should(never()).setEmailError(0);
        then(mView).should(times(3)).setEmailError(anyInt());
    }
}