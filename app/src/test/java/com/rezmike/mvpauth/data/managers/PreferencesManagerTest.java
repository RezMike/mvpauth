package com.rezmike.mvpauth.data.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.rezmike.mvpauth.data.network.res.UserRes;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.data.storage.dto.UserSettingsDto;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static com.rezmike.mvpauth.data.managers.PreferencesManager.DEFAULT_LAST_UPDATE;
import static com.rezmike.mvpauth.data.managers.PreferencesManager.ORDER_NOTIFICATION_KEY;
import static com.rezmike.mvpauth.data.managers.PreferencesManager.PROFILE_AUTH_TOKEN_KEY;
import static com.rezmike.mvpauth.data.managers.PreferencesManager.PROFILE_AVATAR_KEY;
import static com.rezmike.mvpauth.data.managers.PreferencesManager.PROFILE_NAME_KEY;
import static com.rezmike.mvpauth.data.managers.PreferencesManager.PROFILE_PHONE_KEY;
import static com.rezmike.mvpauth.data.managers.PreferencesManager.PROFILE_USER_ID_KEY;
import static com.rezmike.mvpauth.data.managers.PreferencesManager.PROMO_NOTIFICATION_KEY;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class PreferencesManagerTest {

    @Mock
    SharedPreferences mockSharedPreferences;
    @Mock
    SharedPreferences.Editor mockEditor;
    @Mock
    Context mockContext;

    private Map<String, String> fakeStringMap = new HashMap<>();
    private Map<String, Boolean> fakeBooleanMap = new HashMap<>();
    private Map<String, Integer> fakeIntMap = new HashMap<>();

    private PreferencesManager mPreferencesManager;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        given(mockContext.getSharedPreferences(anyString(), anyInt())).willReturn(mockSharedPreferences);
        given(mockSharedPreferences.edit()).willReturn(mockEditor);

        mPreferencesManager = new PreferencesManager(mockContext);
    }

    //region ======================== Prepare stub ========================

    private void preparePutBooleanStub() {
        given(mockEditor.putBoolean(anyString(), anyBoolean())).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            Boolean value = invocation.getArgument(1);
            fakeBooleanMap.put(key, value);
            return null;
        });

        given(mockSharedPreferences.getBoolean(anyString(), anyBoolean())).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            Boolean value = fakeBooleanMap.get(key);
            if (value == null) {
                value = invocation.getArgument(1);
            }
            return value;
        });
    }

    private void preparePutIntStub() {
        given(mockEditor.putInt(anyString(), nullable(Integer.class))).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            Integer value = invocation.getArgument(1);
            fakeIntMap.put(key, value);
            return null;
        });

        given(mockSharedPreferences.getInt(anyString(), nullable(Integer.class))).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            Integer value = fakeIntMap.get(key);
            if (value == null) {
                value = invocation.getArgument(1);
            }
            return value;
        });
    }

    private void preparePutStringStub() {
        given(mockEditor.putString(anyString(), nullable(String.class))).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            String value = invocation.getArgument(1);
            fakeStringMap.put(key, value);
            return null;
        });

        given(mockSharedPreferences.getString(anyString(), nullable(String.class))).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            String value = fakeStringMap.get(key);
            if (value == null) {
                value = invocation.getArgument(1);
            }
            return value;
        });
    }

    //endregion

    @Test
    public void isUserAuth_tokenNotExist_DEFAULT_VALUE_FALSE() throws Exception {
        //given
        preparePutBooleanStub();

        //when
        boolean actualResult = mPreferencesManager.isUserAuth();

        //then
        assertFalse(actualResult);
    }

    @Test
    public void isUserAuth_tokenExist_RESULT_TRUE() throws Exception {
        //given
        preparePutStringStub();
        String expectedToken = "joihrtgbowithb[owithbwrhnoi9230840293rjk3p4oktjki-054t-gkr,g";
        mockEditor.putString(PROFILE_AUTH_TOKEN_KEY, expectedToken);

        //when
        boolean result = mPreferencesManager.isUserAuth();

        //then
        assertTrue(result);
    }

    @Test
    public void getAuthToken_tokenExist_USER_TOKEN_EQ_EXPECTED_TOKEN() throws Exception {
        //given
        preparePutStringStub();
        String expectedToken = "joihrtgbowithb[owithbwrhnoi9230840293rjk3p4oktjki-054t-gkr,g";
        mockEditor.putString(PROFILE_AUTH_TOKEN_KEY, expectedToken);

        //when
        String actualToken = mPreferencesManager.getAuthToken();

        //then
        assertEquals(expectedToken, actualToken);
    }

    @Test
    public void getLastProductUpdate_updateNotExist_DEFAULT_VALUE_DEFAULT_LAST_UPDATE() throws Exception {
        //given
        preparePutStringStub();

        //when
        String actualLastUpdate = mPreferencesManager.getLastProductUpdate();

        //then
        assertEquals(DEFAULT_LAST_UPDATE, actualLastUpdate);
    }

    @Test
    public void getProfileInfo_userExist_EXPECTED_USER_VALUES_EQ_PREFERENCES_VALUES() throws Exception {
        //given
        preparePutStringStub();
        ProfileInfoDto user = new ProfileInfoDto("Резниченко Михаил", "89179716463", "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg");

        //when
        mPreferencesManager.saveProfileInfo(user);

        //then
        then(mockEditor).should(times(1)).apply();
        assertNotNull(mPreferencesManager.getProfileInfo());
        assertEquals(user.getName(), mockSharedPreferences.getString(PROFILE_NAME_KEY, null));
        assertEquals(user.getPhone(), mockSharedPreferences.getString(PROFILE_PHONE_KEY, null));
        assertEquals(user.getAvatar(), mockSharedPreferences.getString(PROFILE_AVATAR_KEY, null));
    }

    @Test
    public void saveProfileInfo_expectedInfoDto_EXPECTED_USER_VALUES_EQ_PREFERENCES_VALUES() throws Exception {
        //given
        preparePutStringStub();
        ProfileInfoDto expectedUser = new ProfileInfoDto("Резниченко Михаил", "89179716463", "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg");

        //when
        mPreferencesManager.saveProfileInfo(expectedUser);

        //then
        then(mockEditor).should(times(1)).apply();
        assertEquals(expectedUser.getName(), mockSharedPreferences.getString(PROFILE_NAME_KEY, null));
        assertEquals(expectedUser.getPhone(), mockSharedPreferences.getString(PROFILE_PHONE_KEY, null));
        assertEquals(expectedUser.getAvatar(), mockSharedPreferences.getString(PROFILE_AVATAR_KEY, null));
    }

    @Test
    public void saveProfileInfo_expectedUserRes_EXPECTED_USER_VALUES_EQ_PREFERENCES_VALUES() throws Exception {
        //given
        preparePutStringStub();
        UserRes expectedUser = new UserRes("58711631a242690011b1b2", "Резниченко Михаил", "", "token", "89179716463", null);

        //when
        mPreferencesManager.saveProfileInfo(expectedUser);

        //then
        then(mockEditor).should(times(1)).apply();
        assertEquals(expectedUser.getId(), mockSharedPreferences.getString(PROFILE_USER_ID_KEY, null));
        assertEquals(expectedUser.getFullName(), mockSharedPreferences.getString(PROFILE_NAME_KEY, null));
        assertEquals(expectedUser.getPhone(), mockSharedPreferences.getString(PROFILE_PHONE_KEY, null));
        assertEquals(expectedUser.getAvatarUrl(), mockSharedPreferences.getString(PROFILE_AVATAR_KEY, null));
        assertEquals(expectedUser.getToken(), mockSharedPreferences.getString(PROFILE_AUTH_TOKEN_KEY, null));
    }

    @Test
    public void getUserName_expectedName_EXPECTED_NAME_EQ_PREFERENCES_VALUES() throws Exception {
        //given
        preparePutStringStub();
        String expectedName = "Макеев Михаил";
        mockEditor.putString(PROFILE_NAME_KEY, expectedName);

        //when
        String actualUserName = mPreferencesManager.getUserName();

        //then
        assertEquals(expectedName, actualUserName);
    }

    @Test
    public void getUserAvatar_expectedUrl_EXPECTED_URL_EQ_PREFERENCES_VALUES() throws Exception {
        //given
        preparePutStringStub();
        String expectedUrl = "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg";
        mockEditor.putString(PROFILE_AVATAR_KEY, expectedUrl);

        //when
        String actualUrl = mPreferencesManager.getUserAvatar();

        //then
        assertEquals(expectedUrl, actualUrl);

    }

    @Test
    public void saveUserAvatar_expectedUrl_EXPECTED_URL_EQ_PREFERENCES_VALUES() throws Exception {
        //given
        preparePutStringStub();
        String expectedUrl = "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg";

        //when
        mPreferencesManager.saveUserAvatar(expectedUrl);

        //then
        verify(mockEditor, times(1)).apply();
        assertEquals(expectedUrl, mockSharedPreferences.getString(PROFILE_AVATAR_KEY, null));
    }

    @Test
    public void getUserSettings_notExist_DEFAULT_VALUES_FALSE() throws Exception {
        //given
        preparePutBooleanStub();

        //when
        UserSettingsDto actualSettings = mPreferencesManager.getUserSettings();

        //then
        assertFalse(actualSettings.isPromoNotification());
        assertFalse(actualSettings.isOrderNotification());
    }

    @Test
    public void getUserSettings_exist_EXPECTED_SETTINGS_FIELDS_EQ_PREFERENCES_VALUES() throws Exception {
        //given
        preparePutBooleanStub();
        mockEditor.putBoolean(ORDER_NOTIFICATION_KEY, true);
        mockEditor.putBoolean(PROMO_NOTIFICATION_KEY, false);

        //when
        UserSettingsDto actualSettings = mPreferencesManager.getUserSettings();

        //then
        assertTrue(actualSettings.isOrderNotification());
        assertFalse(actualSettings.isPromoNotification());
    }

    @Test
    public void saveSettings_expectedSettings_EXPECTED_SETTINGS_FIELDS_EQ_PREFERENCES_VALUES() throws Exception {
        //given
        preparePutBooleanStub();
        UserSettingsDto testSettings = new UserSettingsDto(true, false);

        //when
        mPreferencesManager.saveSettings(testSettings);

        //then
        verify(mockEditor, times(1)).apply();
        assertEquals(testSettings.isOrderNotification(), mockSharedPreferences.getBoolean(ORDER_NOTIFICATION_KEY, false));
        assertEquals(testSettings.isPromoNotification(), mockSharedPreferences.getBoolean(PROMO_NOTIFICATION_KEY, false));
    }
}