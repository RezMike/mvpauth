package com.rezmike.mvpauth.data.managers;

import com.rezmike.mvpauth.data.network.DateStringAdapter;
import com.rezmike.mvpauth.data.network.RestService;
import com.rezmike.mvpauth.data.network.error.AccessError;
import com.rezmike.mvpauth.data.network.error.ApiError;
import com.rezmike.mvpauth.data.network.req.SocialLoginReq;
import com.rezmike.mvpauth.data.network.req.UserLoginReq;
import com.rezmike.mvpauth.data.network.res.FacebookProfileRes;
import com.rezmike.mvpauth.data.network.res.ProductRes;
import com.rezmike.mvpauth.data.network.res.UserRes;
import com.rezmike.mvpauth.data.network.res.VkProfileRes;
import com.rezmike.mvpauth.data.storage.dto.FacebookDataDto;
import com.rezmike.mvpauth.data.storage.dto.VkDataDto;
import com.rezmike.mvpauth.data.storage.realm.ProductRealm;
import com.rezmike.mvpauth.resources.StubEntityFactory;
import com.rezmike.mvpauth.resources.TestResponses;
import com.rezmike.mvpauth.utils.ConstantManager;
import com.squareup.moshi.Moshi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

public class DataManagerTest {

    private Retrofit mRetrofit;
    private RestService mRestService;
    private MockWebServer mMockWebServer;
    private PreferencesManager mPreferencesManager;
    private RealmManager mRealmManager;
    private DataManager mDataManager;

    @Before
    public void setUp() throws Exception {
        prepareMockServer();
        mRestService = mRetrofit.create(RestService.class);
        prepareRxSchedulers(); // for .subscribeOn/.observeOn
        mPreferencesManager = mock(PreferencesManager.class);
        mRealmManager = mock(RealmManager.class);
        mDataManager = new DataManager(mRestService, mPreferencesManager, mRealmManager);
    }

    //region ======================== Prepare ========================

    private void prepareMockServer() {
        mMockWebServer = new MockWebServer();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(mMockWebServer.url("").toString())
                .addConverterFactory(MoshiConverterFactory.create(new Moshi.Builder()
                        .add(new DateStringAdapter())
                        .build()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();
    }

    private void prepareRxSchedulers() {
        RxAndroidPlugins.getInstance().reset();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
    }

    private void prepareDispatcher_200() {
        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                String path = request.getPath();
                if (path.contains("?")) {
                    path = path.substring(0, path.indexOf("?"));
                }
                switch (path) {
                    case "/auth/login":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(TestResponses.SUCCESS_USER_RES_WITH_ADDRESS);
                    case "/products":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setHeader(ConstantManager.LAST_MODIFIED_HEADER, PreferencesManager.DEFAULT_LAST_UPDATE)
                                .setBody(TestResponses.SUCCESS_GET_PRODUCTS);
                    case "/users.get":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(TestResponses.SUCCESS_VK_PROFILE_RES);
                    case "/me":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(TestResponses.SUCCESS_FACEBOOK_PROFILE_RES);
                    case "/socialLogin":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(TestResponses.SUCCESS_USER_RES_WITHOUT_ADDRESS);
                    default:
                        return new MockResponse().setResponseCode(404);
                }
            }
        };

        mMockWebServer.setDispatcher(dispatcher);
    }

    //endregion

    @After
    public void tearDown() throws Exception {
        mMockWebServer.shutdown();
    }

    @Test
    public void loginUser_200_OK() throws Exception {
        //given
        prepareDispatcher_200();
        UserLoginReq stubUserLoginReq = StubEntityFactory.makeStub(UserLoginReq.class);
        UserRes expectedUserRes = StubEntityFactory.makeStub(UserRes.class);
        TestSubscriber<UserRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.loginUser(stubUserLoginReq)
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        UserRes actualRes = subscriber.getOnNextEvents().get(0);

        //then
        subscriber.assertNoErrors();
        assertEquals(expectedUserRes.getFullName(), actualRes.getFullName());
        assertEquals(expectedUserRes.getId(), actualRes.getId());
        assertEquals(expectedUserRes.getToken(), actualRes.getToken());
        assertEquals(expectedUserRes.getPhone(), actualRes.getPhone());
        assertEquals(expectedUserRes.getAvatarUrl(), actualRes.getAvatarUrl());
        assertFalse(actualRes.getAddresses().isEmpty());
        then(mPreferencesManager).should(times(1)).saveProfileInfo(actualRes);
    }

    @Test
    public void loginUser_403_FORBIDDEN() throws Exception {
        //given
        mMockWebServer.enqueue(new MockResponse().setResponseCode(403));
        UserLoginReq stubUserLoginReq = StubEntityFactory.makeStub(UserLoginReq.class);
        TestSubscriber<UserRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.loginUser(stubUserLoginReq)
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        Throwable actualThrow = subscriber.getOnErrorEvents().get(0);

        //then
        subscriber.assertError(AccessError.class);
        assertEquals("Неверный логин или пароль", actualThrow.getMessage());
    }

    @Test
    public void loginUser_500_API_ERROR() throws Exception {
        //given
        mMockWebServer.enqueue(new MockResponse().setResponseCode(500));
        UserLoginReq stubUserLoginReq = StubEntityFactory.makeStub(UserLoginReq.class);
        TestSubscriber<UserRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.loginUser(stubUserLoginReq)
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertError(ApiError.class);
    }

    @Test
    public void getProductFromNetwork_200_RECORD_RESPONSE_TO_REALM_MANAGER() throws Exception {
        //given
        prepareDispatcher_200();
        given(mPreferencesManager.getLastProductUpdate()).willReturn(PreferencesManager.DEFAULT_LAST_UPDATE);
        TestSubscriber<ProductRealm> subscriber = new TestSubscriber<>();

        //when
        mDataManager.getProductsObsFromNetwork()
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertNoErrors();
        subscriber.assertCompleted();
        subscriber.assertNoValues();
        then(mPreferencesManager).should(times(1)).saveLastProductUpdate(anyString());
        then(mRealmManager).should(times(1)).deleteFromRealm(any(), anyString());
        then(mRealmManager).should(times(4)).saveProductResponseToRealm(any(ProductRes.class));
    }

    @Test
    public void getProductFromNetwork_304_NOT_RECORDED_TO_REALM_MANAGER() throws Exception {
        //given
        mMockWebServer.enqueue(new MockResponse().setResponseCode(304));
        given(mPreferencesManager.getLastProductUpdate()).willReturn(PreferencesManager.DEFAULT_LAST_UPDATE);
        TestSubscriber<ProductRealm> subscriber = new TestSubscriber<>();

        //when
        mDataManager.getProductsObsFromNetwork()
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();

        //then
        subscriber.assertNoErrors();
        subscriber.assertCompleted();
        subscriber.assertNoValues();
        then(mRealmManager).should(never()).deleteFromRealm(any(), anyString());
        then(mRealmManager).should(never()).saveProductResponseToRealm(any(ProductRes.class));
    }

    @Test
    public void socialLogin_socialLoginReq_SUCCESS_USER_RES() throws Exception {
        //given
        prepareDispatcher_200();
        SocialLoginReq expectedReq = StubEntityFactory.makeStub(SocialLoginReq.class);
        String expectedToken = "wegfvw;edcnw'lkedm93847983yuhefoij32lkml'kjvj30fewoidvn";
        TestSubscriber<UserRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.socialLogin(expectedReq)
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        UserRes actualRes = subscriber.getOnNextEvents().get(0);

        //then
        subscriber.assertNoErrors();
        subscriber.assertCompleted();
        assertEquals(expectedReq.getFullName(), actualRes.getFullName());
        assertEquals(expectedReq.getAvatarUrl(), actualRes.getAvatarUrl());
        assertEquals(expectedReq.getPhone(), actualRes.getPhone());
        assertEquals(expectedToken, actualRes.getToken());
    }

    @Test
    public void getVkProfile_200_SUCCESS_RESPONSE() throws Exception {
        //given
        prepareDispatcher_200();
        VkDataDto expectedVkData = new VkDataDto("anyToken", "anyUserId", "anyEmail");
        String expectedName = "Резниченко Михаил";
        String expectedAvatar = "https://pp.userapi.com/c618628/v618628383/22ceb/2HD4uqLnwRI.jpg";
        String expectedPhone = "89131234567";
        TestSubscriber<VkProfileRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.getVkProfile(mMockWebServer.url("") + "users.get?fields=photo_200,contacts", expectedVkData)
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        VkProfileRes actualRes = subscriber.getOnNextEvents().get(0);

        //then
        subscriber.assertNoErrors();
        subscriber.assertCompleted();
        assertEquals(expectedName, actualRes.getFullName());
        assertEquals(expectedAvatar, actualRes.getAvatar());
        assertEquals(expectedPhone, actualRes.getPhone());
    }

    @Test
    public void getFacebookProfile_200_SUCCESS_RESPONSE() throws Exception {
        //given
        prepareDispatcher_200();
        FacebookDataDto expectedFacebookData = new FacebookDataDto("anyToken", "anyUserId");
        String expectedEmail = "mihail.reznichenko.98@mail.ru";
        String expectedAvatar = "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/11061314_1601303153449528_9222009920332774553_n.jpg?oh=caca35a6ca35d8ef32f46a4f76be94ed&oe=5989D9D2";
        String expectedFirstName = "Михаил";
        String expectedLastName = "Резниченко";
        TestSubscriber<FacebookProfileRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.getFacebookProfile(mMockWebServer.url("") + "me?fields=email,picture.width(200).height(200),first_name,last_name", expectedFacebookData)
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();
        FacebookProfileRes actualRes = subscriber.getOnNextEvents().get(0);

        //then
        subscriber.assertNoErrors();
        subscriber.assertCompleted();
        assertEquals(expectedEmail, actualRes.getEmail());
        assertEquals(expectedAvatar, actualRes.getAvatar());
        assertEquals(expectedFirstName, actualRes.getFirstName());
        assertEquals(expectedLastName, actualRes.getLastName());
    }
}