package com.rezmike.mvpauth.resources;

import android.view.MenuItem;

import com.rezmike.mvpauth.R;
import com.rezmike.mvpauth.data.network.req.SocialLoginReq;
import com.rezmike.mvpauth.data.network.req.UserLoginReq;
import com.rezmike.mvpauth.data.network.res.AddressRes;
import com.rezmike.mvpauth.data.network.res.UserRes;
import com.rezmike.mvpauth.data.storage.dto.ProfileInfoDto;
import com.rezmike.mvpauth.ui.other.MenuItemHolder;

import java.util.Arrays;

import static org.mockito.Mockito.mock;

public class StubEntityFactory {

    @SuppressWarnings("unchecked")
    public static <T> T makeStub(Class<T> stubEntityClass) {
        switch (stubEntityClass.getSimpleName()) {
            case "UserRes":
                return (T) new UserRes("58711631a242690011b1b26d",
                        "Резниченко Михаил",
                        "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg",
                        "wegfvw;edcnw'lkedm93847983yuhefoij32lkml'kjvj30fewoidvn",
                        "89179711111",
                        Arrays.asList(new AddressRes("58711631a242690011b1b26d",
                                "Работа", "Автостроителей", "24", "1", 1, "lorem")));
            case "UserLoginReq":
                return (T) new UserLoginReq("email@mail.ru", "password");
            case "ProfileInfoDto":
                return (T) new ProfileInfoDto("Резниченко Михаил", "89179716463", "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg");
            case "MenuItemHolder":
                return (T) new MenuItemHolder("Редактировать", R.drawable.ic_edit_black_24dp, mock(MenuItem.OnMenuItemClickListener.class));
            case "SocialLoginReq":
                return (T) new SocialLoginReq("Михаил", "Резниченко", "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg", "email@mail.ru", "89179711111");
            default:
                return null;
        }
    }
}
