package com.rezmike.mvpauth.resources;

public class TestResponses {

    public static final String SUCCESS_USER_RES_WITH_ADDRESS = "{\n" +
            "  \"_id\": \"58711631a242690011b1b26d\",\n" +
            "  \"fullName\": \"Резниченко Михаил\",\n" +
            "  \"avatarUrl\": \"https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg\",\n" +
            "  \"token\": \"wegfvw;edcnw'lkedm93847983yuhefoij32lkml'kjvj30fewoidvn\",\n" +
            "  \"phone\": \"89179711111\",\n" +
            "  \"addresses\": [\n" +
            "    {\n" +
            "      \"_id\": \"58711631a242690011b1b26d\",\n" +
            "      \"name\": \"Работа\",\n" +
            "      \"street\": \"Автостроителей\",\n" +
            "      \"house\": \"24\",\n" +
            "      \"apartment\": \"1\",\n" +
            "      \"floor\": 1,\n" +
            "      \"comment\": \"lorem\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public static final String SUCCESS_USER_RES_WITHOUT_ADDRESS = "{\n" +
            "  \"_id\": \"58711631a242690011b1b26d\",\n" +
            "  \"fullName\": \"Резниченко Михаил\",\n" +
            "  \"avatarUrl\": \"https://pp.userapi.com/c313129/v313129097/80ff/5U-iWku.jpg\",\n" +
            "  \"token\": \"wegfvw;edcnw'lkedm93847983yuhefoij32lkml'kjvj30fewoidvn\",\n" +
            "  \"phone\": \"89179711111\",\n" +
            "  \"addresses\": []\n" +
            "}";

    public static final String SUCCESS_GET_PRODUCTS = "[\n" +
            "              {\n" +
            "                \"_id\": \"584f93e2ed0d00001179ac8b\",\n" +
            "                \"productName\": \"test 3\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-03.jpg\",\n" +
            "                \"description\": \"description 3 description 3 description 3 description 3 description 3\",\n" +
            "                \"price\": 300,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f9429ed0d00001179ac8d\",\n" +
            "                \"productName\": \"test 5\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-05.jpg\",\n" +
            "                \"description\": \"description 5 description 5 description 5 description 5 description 5\",\n" +
            "                \"price\": 100,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": false\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f9445ed0d00001179ac8f\",\n" +
            "                \"productName\": \"test 7\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-07.jpg\",\n" +
            "                \"description\": \"description 7 description 7 description 7 description 7 description 7\",\n" +
            "                \"price\": 600,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": []\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"58712357a242690011b1b278\",\n" +
            "                \"productName\": \"test 8\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-08.jpg\",\n" +
            "                \"description\": \"description 8 description 8 description 8 description 8 description 8\",\n" +
            "                \"price\": 600,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f944fed0d00001179ac90\",\n" +
            "                \"productName\": \"test 9\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-08.jpg\",\n" +
            "                \"description\": \"description 8 description 8 description 8 description 8 description 8\",\n" +
            "                \"price\": 900,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": false,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": false\n" +
            "                  }\n" +
            "                ]\n" +
            "              }\n" +
            "            ]";

    public static final String SUCCESS_VK_PROFILE_RES = "{\n" +
            "  \"response\": [\n" +
            "    {\n" +
            "      \"uid\": 165804383,\n" +
            "      \"first_name\": \"Михаил\",\n" +
            "      \"last_name\": \"Резниченко\",\n" +
            "      \"photo_200\": \"https://pp.userapi.com/c618628/v618628383/22ceb/2HD4uqLnwRI.jpg\",\n" +
            "      \"mobile_phone\": \"89131234567\", \n" +
            "      \"home_phone\": \"\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public static final String SUCCESS_FACEBOOK_PROFILE_RES = "{\n" +
            "  \"email\": \"mihail.reznichenko.98@mail.ru\",\n" +
            "  \"picture\": {\n" +
            "    \"data\": {\n" +
            "      \"height\": 200,\n" +
            "      \"is_silhouette\": false,\n" +
            "      \"url\": \"https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/11061314_1601303153449528_9222009920332774553_n.jpg?oh=caca35a6ca35d8ef32f46a4f76be94ed&oe=5989D9D2\",\n" +
            "      \"width\": 200\n" +
            "    }\n" +
            "  },\n" +
            "  \"first_name\": \"Михаил\",\n" +
            "  \"last_name\": \"Резниченко\",\n" +
            "  \"id\": \"1929073574005816\"\n" +
            "}";
}
